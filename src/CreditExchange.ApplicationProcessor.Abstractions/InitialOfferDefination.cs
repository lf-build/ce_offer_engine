﻿using System.Collections.Generic;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence;
using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using System;

namespace CreditExchange.ApplicationProcessor
{
    public class InitialOfferDefination : Aggregate, IInitialOfferDefination
    {
        public string EntityType { get; set; }
        public string EntityId { get; set; }
        public  ScoreCardApplicationStatus Status { get; set; }
        public List<string> Reasons { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<IInitialOffer, InitialOffer>))]
        public List<IInitialOffer> Offers { get; set; }
        public TimeBucket GeneratedOn { get; set; }
        public DateTimeOffset OfferSelectionDate { get; set; }

        public bool IsAvailable { get; set; }
    }
}
