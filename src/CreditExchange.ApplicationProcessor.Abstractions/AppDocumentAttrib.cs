﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CreditExchange.ApplicationProcessor
{
    public class AppDocumentAttrib
    {
        public string TemplateName { get; set; }
        public string TemplateVersion { get; set; }
        public string DocumentName { get; set; }
        public string Category { get; set; }
        public List<string> Tags { get; set; }
    }
}
