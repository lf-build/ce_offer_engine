﻿namespace CreditExchange.ApplicationProcessor.Events
{
    public class FinalOfferProcessed
    {
        public IFinalOfferDefination FinalOffer { get; set; }
    }
}
