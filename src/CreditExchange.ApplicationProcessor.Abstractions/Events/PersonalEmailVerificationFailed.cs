﻿namespace CreditExchange.ApplicationProcessor.Events
{
    public class PersonalEmailVerificationFailed
    {
        public string EntityType { get; set; }
        public string ApplicationNumber { get; set; }

        public object SyndicationResult { get; set; }

        public string EmailValidationProvider { get; set; }

        public string EmailAddress { get; set; }

    }
}
