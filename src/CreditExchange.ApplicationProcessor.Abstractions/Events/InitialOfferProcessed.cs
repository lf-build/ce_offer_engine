﻿namespace CreditExchange.ApplicationProcessor.Events
{
    public class InitialOfferProcessed
    {
        public IInitialOfferDefination InitialOffer { get; set; }
    }
}
