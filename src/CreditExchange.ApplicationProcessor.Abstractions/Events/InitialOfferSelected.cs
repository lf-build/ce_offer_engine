﻿
namespace CreditExchange.ApplicationProcessor
{
    public class InitialOfferSelected 
    {
        public string EntityType { get; set; }
        public string EntityId { get; set; }
        public bool IsAccepted { get; set; }
        public IInitialOffer SelectedInitialOffer { get; set; }
        public string CibilScore { get; set; }
        public string EcnNumber { get; set; }
    }
}
