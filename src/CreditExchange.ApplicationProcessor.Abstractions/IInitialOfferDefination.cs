﻿using System.Collections.Generic;
using LendFoundry.Foundation.Persistence;
using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using LendFoundry.Foundation.Date;
using System;

namespace CreditExchange.ApplicationProcessor
{
    public interface IInitialOfferDefination : IAggregate
    {
        string EntityType { get; set; }
        string EntityId { get; set; }
        ScoreCardApplicationStatus Status { get; set; }
        List<string> Reasons { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<IInitialOffer, InitialOffer>))]
        List<IInitialOffer> Offers { get; set; }
        TimeBucket GeneratedOn { get; set; }

        DateTimeOffset OfferSelectionDate { get; set; }
        bool IsAvailable { get; set; }
    }

}
