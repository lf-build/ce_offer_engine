﻿using System.Collections.Generic;
using CreditExchange.Applicant;

namespace CreditExchange.ApplicationProcessor {
    public class ApplicationAttribute {
        public double requestedAmount { get; set; }
        public double selfEmi { get; set; }
        public double creditCardBalance { get; set; }
        public string purposeOfLoan { get; set; }
        public ApplicationDate applicationDate { get; set; }
        public int age { get; set; }
        public string city { get; set; }
        public string zipCode { get; set; }
        public double income { get; set; }
        public string employeeType { get; set; }
        public string pan { get; set; }
        public string firstName { get; set; }
        public string middleName { get; set; }
        public string lastName { get; set; }
        public string gender { get; set; }
        public string maritalStatus { get; set; }
        public object landlineNo { get; set; }
        public string mobileNo { get; set; }
        public List<Address> addresses { get; set; }
        public double monthlyExpense { get; set; }
        public double monthlyRent { get; set; }
        public string residenceType { get; set; }
        public string cinNumber { get; set; }
        public string companyEmailAddress { get; set; }
        public string companyName { get; set; }
        public string dateOfBirth { get; set; }
        public List<EmploymentAddress> employmentAddress { get; set; }
        public string designation { get; set; }
        public int totalWorkExp { get; set; }
        public string personalEmail { get; set; }
        public string aadharNumber { get; set; }
        public object educationalQualification { get; set; }
        public object educationalInstitution { get; set; }
        public string salutation { get; set; }
        public List<BankInformation> bankInformation { get; set; }
        public string requestedTermType { get; set; }
        public double requestedTermValue { get; set; }
        public Source source { get; set; }
        public CurrentAddress currentAddress { get; set; }
        public PermanentAddress permanentAddress { get; set; }
        public ExpiryDate expiryDate { get; set; }
        public double OtherEmi { get; set; }

        public string customIncome { get; set; }

        public string customApplicationDate { get; set; }
        string CurrentAddressYear { get; set; }
        string CurrentAddressMonth { get; set; }
        string EmployementYear { get; set; }
        string EmployementMonth { get; set; }

    }

    public class ApplicationDate {
        public string Time { get; set; }
        public string Hour { get; set; }
        public string Day { get; set; }
        public string Week { get; set; }
        public string Month { get; set; }
        public string Quarter { get; set; }
        public string Year { get; set; }
    }
    public class EmploymentAddress {
        public object AddressId { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string AddressLine3 { get; set; }
        public object AddressLine4 { get; set; }
        public object LandMark { get; set; }
        public object Location { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string PinCode { get; set; }
        public string Country { get; set; }
        public string AddressType { get; set; }
        public bool IsDefault { get; set; }
    }

    public class BankAddresses {
        public string AddressId { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string AddressLine3 { get; set; }
        public string AddressLine4 { get; set; }
        public string LandMark { get; set; }
        public string Location { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string PinCode { get; set; }
        public string Country { get; set; }
        public string AddressType { get; set; }
        public bool IsDefault { get; set; }
    }
    public class Source {
        public string TrackingCode { get; set; }
        public string SystemChannel { get; set; }
        public string SourceType { get; set; }
        public object SourceReferenceId { get; set; }
    }

    public class CurrentAddress {
        public string AddressId { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public object AddressLine3 { get; set; }
        public object AddressLine4 { get; set; }
        public object LandMark { get; set; }
        public string Location { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string PinCode { get; set; }
        public string Country { get; set; }
        public string AddressType { get; set; }
        public bool IsDefault { get; set; }
    }

    public class PermanentAddress {
        public string AddressId { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public object AddressLine3 { get; set; }
        public object AddressLine4 { get; set; }
        public object LandMark { get; set; }
        public string Location { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string PinCode { get; set; }
        public string Country { get; set; }
        public string AddressType { get; set; }
        public bool IsDefault { get; set; }
    }

    public class ExpiryDate {
        public string Time { get; set; }
        public string Hour { get; set; }
        public string Day { get; set; }
        public string Week { get; set; }
        public string Month { get; set; }
        public string Quarter { get; set; }
        public string Year { get; set; }
    }
}