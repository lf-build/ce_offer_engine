﻿namespace CreditExchange.ApplicationProcessor
{
    public interface IInitialOfferSelected
    {
        string EntityId { get; set; }
        string EntityType { get; set; }
        bool IsAccepted { get; set; }
        string OfferId { get; set; }
    }
}