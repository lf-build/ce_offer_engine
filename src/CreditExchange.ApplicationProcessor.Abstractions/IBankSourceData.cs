﻿namespace CreditExchange.ApplicationProcessor
{
    public interface IBankSourceData
    {

         string BankSourceType { get; set; }

         object BankReport { get; set; }

    }
}
