﻿using LendFoundry.Foundation.Client;
using Newtonsoft.Json;

namespace CreditExchange.ApplicationProcessor
{
    public interface IApplicationExtension
    {
        [JsonConverter(typeof(InterfaceConverter<IMobileVerificationRequest, MobileVerificationRequest>))]
        IMobileVerificationRequest MobileVerificationDetail { get; set; }

         string EmailVerificationResult { get; set; }

         string EmailVerificationProvider { get; set; }
    }
}
