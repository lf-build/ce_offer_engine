﻿namespace CreditExchange.ApplicationProcessor
{
    public interface IInitialOffer
    {
        string OfferId { get; set; }
        string FileType { get; set; }
        double InterestRate { get; set; }
        int LoanTenure { get; set; }
        double OfferAmount { get; set; }
        double Score { get; set; }
        bool IsSelected { get; set; }
        string RejectCode { get; set; }
        
         double FinalOfferAmount { get; set; }
         double MaxPermittedEMI { get; set; }
         double netMonthlyIncome { get; set; }
         double currentEMINMI { get; set; }
         double monthlyExpense { get; set; }

        bool IsManualOffer { get; set; }
    }
}