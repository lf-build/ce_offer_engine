﻿using System.Threading.Tasks;

namespace CreditExchange.ApplicationProcessor
{
    public interface IOrchestrationService
    {
        Task<IInitialOfferDefination> RunInitialOffer( string applicationNumber,bool isReCompute);
        Task<IFinalOfferDefination> RunFinalOffer(string applicationNumber, bool isReCompute);
    }
}
