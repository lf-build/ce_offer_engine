﻿using System;

namespace CreditExchange.ApplicationProcessor
{
   public interface IPerfiosReportData
    {
         double MonthlyEmi { get; set; }
         double MonthlyExpense { get; set; }
         double MonthlyIncome { get; set; }
         double AvgInvestmentExpensePerMonth { get; set; }
         double AvgDailyBalance { get; set; }
         DateTimeOffset SalaryDate { get; set; }
         double EcsBounce { get; set; }
         string ReferenceNumber { get; set; }

    }
}
