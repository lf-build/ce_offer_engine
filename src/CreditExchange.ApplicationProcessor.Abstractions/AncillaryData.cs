﻿namespace CreditExchange.ApplicationProcessor
{
    public class AncillaryData : IAncillaryData
    {
        public string Category { get; set; }
        public string Religion { get; set; }
        public string MothersMaidenName { get; set; }
        public string DocumentDate { get; set; }
        public decimal CurrentResidenceYears { get; set; }
        public decimal CurrentCityYears { get; set; }
        public decimal CurrentWorkExperience { get; set; }
        public string KYCDocumentNumber { get; set; }

        public string KYCDocumentType { get; set; }

        public int DependentsCount { get; set; }

        public string FatherOrSpouseName { get; set; }
    }
}
