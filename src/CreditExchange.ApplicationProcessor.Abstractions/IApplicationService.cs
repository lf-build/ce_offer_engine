﻿using System;
using System.Threading.Tasks;
using CreditExchange.Applicant;
using CreditExchange.Application;
using CreditExchange.Applications.Filters.Abstractions;
using CreditExchange.Syndication.Perfios.Response;

namespace CreditExchange.ApplicationProcessor {
    public interface IApplicationService {
        //Task<IConsent> SignLoanAggrement(string applicationNumber);
        //Task<IConsent> LoanApplicationForm(string applicationNumber);
        Task<Tuple<IApplicationResponse, string>> SubmitApplication (ISubmitApplicationRequest submitApplicationRequest);

        Task<IAncillaryData> UpdateAncillaryData (string applicationNumber, IAncillaryData ancillaryData);

        Task<ILoanFundedData> UpdateLoanFundedData (string applicationNumber, ILoanFundedData loanFundedData);

        Task<object> UpdatePerfiosData (string applicationNumber, object cashFlowReportData);

        Task<IApplicationResponse> AddLead (ISubmitApplicationRequest submitApplicationRequest);

        Task<IUploadStatementResponse> UploadBankStatement (string entityType, string entityId, string fileName, byte[] fileBytes, string password);

        Task<Tuple<IApplication, string>> UpdateApplication (string applicationNumber, ISubmitApplicationRequest submitApplicationRequest);

        Task UpdateEmploymentDetail (string applicationNumber, IEmploymentDetail employmentInformationRequest);

        Task UpdateSelfDeclaredExpense (string applicationNumber, IExpenseDetailRequest selfDeclareExpense);

        Task UpdateEmailInformation (string applicationNumber, IEmailAddress emailAddress);

        Task UpdateBankInformation (string applicationNumber, IBankInformation bankInformationRequest);

        Task UpdateEducationInformation (string applicationNumber, IEducationInformation bankInformationRequest);

        Task UpdatePersonalDetails (string applicationNumber, UpdateApplicantRequest personalDetails);

        Task UpdateAddress (string applicationNumber, IAddress address);

        Task<string> UpdateStatus (string applicationNumber);

        Task<string> UpdateBankSourceData (string applicationNumber, string sourceType);

        Task<IBankSourceData> GetBankSourceData (string applicationNumber);

        Task EnableReApplyInCoolOff (string applicationNumber, string applicantId);

        Task UpdateLoanAmount (string applicationNumber, IUpdateLoanAmountRequest updateLoanAmountRequest);

        Task<IEmailAddress> ReInitiateEmailVerification (string applicationNumber, IEmailAddress emailAddress);

        Task<bool> CheckIfDuplicateApplicationExists (UniqueParameterTypes parameterName, string parameterValue, string applicationNumber);

        Task UpdateLoanPurpose (string applicationNumber, string loanPurpose, string otherLoanPurpose);

        Task<bool> IsValidEmail (string Email);

        Task UpdateAadhaarnumber (string applicationNumber, string aadhaarNumber);

        Task<IApplicationResponse> AddLeadForNewBorrowerPortal (ISubmitApplicationRequest submitApplicationRequest);
        Task<Tuple<IApplication, string>> UpdatePartialApplicationData (string applicationNumber, ISubmitApplicationRequest submitApplicationRequest);
        Task UpdateEmploymentDetail (string applicationNumber, IEmploymentDetail employmentInformationRequest, string employementYear, string employementMonth);
    }
}