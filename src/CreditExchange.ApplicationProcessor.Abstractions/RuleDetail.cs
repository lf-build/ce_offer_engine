﻿namespace CreditExchange.ApplicationProcessor
{
    public class RuleDetail
    {
        public string Name { get; set; }
        public string Type { get; set; }
        public string Group { get; set; }
    }
}