﻿namespace CreditExchange.ApplicationProcessor
{
    public class CompanyCategory
    {
        public string companyCategory { get; set; }
        public string referenceNumber { get; set; }
        public string experianCategory { get; set; }
        public string subCategory { get; set; }
        public string typeofcompany { get; set; }
        public string cin { get; set; }
        public string category { get; set; }
    }
}
