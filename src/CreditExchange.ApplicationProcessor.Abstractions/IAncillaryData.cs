﻿namespace CreditExchange.ApplicationProcessor
{
    public interface IAncillaryData
    {

        string Category { get; set; }
        string Religion { get; set; }
        string MothersMaidenName { get; set; }

        string DocumentDate { get; set; }

        decimal CurrentResidenceYears { get; set; }
        decimal CurrentCityYears { get; set; }

        decimal CurrentWorkExperience { get; set; }

        string KYCDocumentType { get; set; }
        string KYCDocumentNumber { get; set; }

        int DependentsCount { get; set; }

        string FatherOrSpouseName { get; set; }

    }
}
