﻿namespace CreditExchange.ApplicationProcessor
{
    public interface IFinalOffer
    {
        string OfferId { get; set; }
        string Loantenure { get; set; }
        double OfferAmount { get; set; }
        double FinalOfferAmount { get; set; }
        double NetMonthlyIncome { get; set; }
        double CurrentEMINMI { get; set; }
        double MaxPermittedEMI { get; set; }
        double Emi { get; set; }
        int ProcessingFee { get; set; }
        double ProcessingFeePercentage { get; set; }
        double CalculatedProcessingFee { get; set; }
        double InterestRate { get; set; }
        bool ManualReview { get; set; }
        bool IsSelected { get; set; }
        string RejectCode { get; set; }
        //Indicates offer is added manually and not generated through offer generation mechanisam
        bool IsManualOffer { get; set; }
        bool IsPresented { get; set; }
    }
}