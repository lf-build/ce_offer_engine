﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CreditExchange.CompanyDb.Abstractions;
using LendFoundry.DataAttributes;
using CreditExchange.GeoProfile;
using CreditExchange.ScoreCard;
using CreditExchange.Syndication.Lenddo;
using Newtonsoft.Json.Linq;
using Gender = CreditExchange.Applicant.Gender;
using LendFoundry.StatusManagement;
using CreditExchange.Syndication.ViewDNS;
using LendFoundry.Foundation.Logging;
using CreditExchange.Perfios;
using LendFoundry.Foundation.Date;
using CreditExchange.Syndication.Perfios.Response;
using LendFoundry.EmailVerification;
using LendFoundry.VerificationEngine;
using CreditExchange.Syndication.Cibil.Request;
using CreditExchange.Syndication.Cibil.Response;
using Microsoft.CSharp.RuntimeBinder;
using System.Globalization;
using System.Threading;
using CreditExchange.Lenddo.Abstractions;
using CreditExchange.Applicant;
using LendFoundry.Foundation.Services;
using Newtonsoft.Json;
using CreditExchange.Cibil;

namespace CreditExchange.ApplicationProcessor
{
    public class OrchestrationService : IOrchestrationService
    {
        public OrchestrationService(
            //ICrifCreditReportService crifCreditReportService
            //, ICrifPanVerificationService crifPanVerificationService
            //, 
            IGeoProfileService geoProfileService
            , IInitialOfferEngineRepository offerEngineRepository, IDataAttributesEngine dataAttributesEngine
            , IScoreCardService scoreCardService
            , IFinalOfferEngineRepository finalOfferEngineRepository
            , IPerfiosService perfiosService
            , ICompanyService companyService
            , ILenddoService lenddoService
            , IEntityStatusService entityStatusService
            , IViewDnsService viewDnsService
            , ILogger logger
            , ITenantTime tenantTime
            , IEmailVerificationService emailVerificationService
            , IVerificationEngineService verificationEngineService
            , ApplicationProcessorConfiguration applicationProcessorConfiguration
            , ICibilReportService cibilReportService,
            Application.IApplicationService applicationService, IApplicantService applicantService)//,IApplicationService applicationService)
        {
            DataAttributesEngine = dataAttributesEngine;
            OfferEngineRepository = offerEngineRepository;
            GeoProfileService = geoProfileService;
            //CrifCreditReportService = crifCreditReportService;
            //CrifPanVerificationService = crifPanVerificationService;
            ScoreCardService = scoreCardService;
            FinalOfferEngineRepository = finalOfferEngineRepository;
            PerfiosService = perfiosService;
            CompanyService = companyService;
            LenddoService = lenddoService;
            EntityStatusService = entityStatusService;
            ViewDnsService = viewDnsService;
            Logger = logger;
            EmailVerificationService = emailVerificationService;
            TenantTime = tenantTime;
            VerificationEngineService = verificationEngineService;
            ApplicationProcessorConfiguration = applicationProcessorConfiguration;
            CibilReportService = cibilReportService;
            ExternalApplicationService = applicationService;
            ApplicantService = applicantService;
            //  ApplicationService = applicationService;
        }

        #region "Private Member"
        private ApplicationProcessorConfiguration ApplicationProcessorConfiguration { get; }
        //private ICrifCreditReportService CrifCreditReportService { get; }
        //private ICrifPanVerificationService CrifPanVerificationService { get; }
        private IScoreCardService ScoreCardService { get; }
        private IInitialOfferEngineRepository OfferEngineRepository { get; }
        private IGeoProfileService GeoProfileService { get; }
        private IDataAttributesEngine DataAttributesEngine { get; }
        private IFinalOfferEngineRepository FinalOfferEngineRepository { get; }
        private IPerfiosService PerfiosService { get; }
        private ILenddoService LenddoService { get; }
        private ICompanyService CompanyService { get; }

        private IEntityStatusService EntityStatusService { get; }
        private IViewDnsService ViewDnsService { get; }
        private ILogger Logger { get; }
        private ITenantTime TenantTime { get; }
        private IEmailVerificationService EmailVerificationService { get; }
        private IVerificationEngineService VerificationEngineService { get; }

        // private IApplicationService ApplicationService { get; }
        private ICibilReportService CibilReportService { get; }
        private Application.IApplicationService ExternalApplicationService { get; }
        #endregion
        private IApplicantService ApplicantService { get; }
        public async Task<IFinalOfferDefination> RunFinalOffer(string applicationNumber, bool isReCallSyndication)
        {
            var isManualReview = false;

            var finalOfferResult = new FinalOfferDefination
            {
                EntityId = applicationNumber,
                EntityType = "application"
            };
            try
            {
                var applicantInformation = await GetApplicationInformation(applicationNumber);

                var perfiosReferenceNumber = CallPerfiosCreditReportSyndication(applicationNumber, isReCallSyndication).Result;


                //var companyDbResponse = CallCompanyDbSyndication(applicationNumber, applicantInformation.CinNumber);
                try
                {
                    var lendoResponse = CallLenddoSyndication(applicationNumber);
                }
                catch (Exception ex)
                {
                    Logger.Error(ex.Message);
                }

                Dictionary<string, string> referenceNumbers = null;
                if (!string.IsNullOrEmpty(perfiosReferenceNumber))
                {
                    VerifyPerfiosInformation(applicationNumber);
                    referenceNumbers = new Dictionary<string, string>();
                    if (!string.IsNullOrEmpty(perfiosReferenceNumber))
                    {
                        referenceNumbers.Add("perfiosReport", perfiosReferenceNumber);
                    }

                }

                VerifyRule(applicationNumber, "ScoringCritiria", "ScoringRules", "FinalOfferScoreCalculation", referenceNumbers);

                var finalOfferScoreRuleResult = VerifyRule(applicationNumber, "OfferCritiria", "FinalOfferCalculation", "FinalOfferCalculation", null);

                var isEligible = finalOfferScoreRuleResult.Result;

                if (isEligible)
                {
                    var intermidiateResult =
                        JObject.FromObject(finalOfferScoreRuleResult.IntermediateData)
                            .ToObject<Dictionary<string, object>>();
                    var finalOffer = JArray.FromObject(intermidiateResult["offerData"]).ToObject<List<FinalOffer>>();
                    finalOfferResult.FinalOffers = finalOffer.ConvertAll(o => (IFinalOffer)o);
                    finalOfferResult.GeneratedOn = new TimeBucket(TenantTime.Now);

                    foreach (var offer in finalOffer)
                    {
                        isManualReview = offer.ManualReview;
                        offer.OfferId = Guid.NewGuid().ToString("N");
                    }
                    finalOfferResult.Status = ScoreCardApplicationStatus.Completed;

                    var workEmailStatus = await VerificationEngineService.GetStatus("application", applicationNumber, "WorkEmailVerification");
                    if (workEmailStatus != null && workEmailStatus.VerificationStatus != VerificationStatus.Passed)
                    {
                        finalOfferResult.Status = ScoreCardApplicationStatus.Completed;
                        finalOfferResult.Reasons = new List<string> { "Work Email Verification Failed" };
                    }
                }
                else
                {
                    finalOfferResult.Status = ScoreCardApplicationStatus.Rejected;
                    finalOfferResult.Reasons = finalOfferScoreRuleResult.ResultDetail;
                }
                if (isManualReview)
                {
                    finalOfferResult.Status = ScoreCardApplicationStatus.ManualReview;
                    finalOfferResult.Reasons = finalOfferScoreRuleResult.ResultDetail;
                }
            }
            catch (OfferDeclinedException exception)
            {
                finalOfferResult.Status = ScoreCardApplicationStatus.Rejected;
                finalOfferResult.Reasons = exception.ScoreCardResult.ResultDetail;
            }
            catch (OfferGenerationFailedException exception)
            {
                finalOfferResult.Status = ScoreCardApplicationStatus.Failed;
                finalOfferResult.Reasons = new List<string> { exception.Message };
            }
            catch (Exception exception)
            {
                finalOfferResult.Status = ScoreCardApplicationStatus.Failed;
                finalOfferResult.Reasons = new List<string>() { exception.Message };
            }
            finalOfferResult.IsAvailable = true;
            await FinalOfferEngineRepository.AddOffer("application", applicationNumber, finalOfferResult);
            //FinalOfferEngineRepository.Add(finalOfferResult);
            //TODO: What about reason
            //ToDo : Rules will return the reason code which will match with status management

            return finalOfferResult;
        }

        public async Task<IInitialOfferDefination> RunInitialOffer(string applicationNumber, bool isReCallSyndication)
        {
            Logger.Info($"[OrchestrationService] RunInitialOffer method  execution started for [{applicationNumber}]");
            var initialOfferResult = new InitialOfferDefination
            {
                EntityId = applicationNumber,
                EntityType = "application"
            };

            try
            {
                Logger.Info($"[OrchestrationService/RunInitialOffer] GetApplicationInformation execution started for [{applicationNumber}]");
                var applicantInformation = await GetApplicationInformation(applicationNumber);
                Logger.Info($"[OrchestrationService/RunInitialOffer] GetApplicationInformation execution finished for [{applicationNumber}] result :  [{(applicantInformation != null ? JsonConvert.SerializeObject(applicantInformation) : null)}] ");

                Logger.Info($"[OrchestrationService/RunInitialOffer] VerifyRule EligibilityCriteria execution started for [{applicationNumber}]");
                VerifyRule(applicationNumber, "EligibilityCriteria", "Prequalification", "ScoreCardEligibilityRule", null);
                Logger.Info($"[OrchestrationService/RunInitialOffer] VerifyRule EligibilityCriteria execution finished for [{applicationNumber}]");
                var primaryBuerua = ApplicationProcessorConfiguration.PrimaryCreditBureau;

                primaryBuerua = string.IsNullOrEmpty(primaryBuerua) ? "CIBIL" : primaryBuerua;

                var crifCreditReportReferenceNumber = CallCreditReportSyndication(applicationNumber,
                    applicantInformation, primaryBuerua, isReCallSyndication);

                //VerifyPan(applicationNumber, applicantInformation, isReCallSyndication);

                var geoProfileReferenceNumber = CallGeoProfileSyndication(applicationNumber, applicantInformation.ZipCode, isReCallSyndication);

                Dictionary<string, string> referenceNumbers = new Dictionary<string, string>();

                if (!string.IsNullOrEmpty(crifCreditReportReferenceNumber))
                {
                    referenceNumbers.Add("crifCreditReport", crifCreditReportReferenceNumber);
                }
                if (!string.IsNullOrEmpty(geoProfileReferenceNumber))
                {
                    referenceNumbers.Add("geoRiskPinCode", geoProfileReferenceNumber);
                }
                VerifyGeoProfile(applicationNumber, referenceNumbers);

                VerifyCreditReportEligibility(applicationNumber);

                // CompanyDb Verification
                try
                {
                    CallCompanyDbSyndication(applicationNumber, applicantInformation.CinNumber, isReCallSyndication);
                }
                catch (Exception ex)
                {
                    Logger.Info(ex.Message);
                }

                Dictionary<string, string> referenceNumber = new Dictionary<string, string>();
                //if (!string.IsNullOrEmpty(crifCreditReportReferenceNumber))
                //{
                //    referenceNumber.Add("crifCreditReport", crifCreditReportReferenceNumber);
                //}

                var initialOfferRuleResult = VerifyRule(applicationNumber, "OfferCritiria",
                    "InitialOfferCalculation", "CalculateInitialOffer", referenceNumber);

                var intermidiateResult =
                    JObject.FromObject(initialOfferRuleResult.IntermediateData)
                        .ToObject<Dictionary<string, object>>();

                var initialOffer =
                    JArray.FromObject(intermidiateResult["offerData"]).ToObject<List<InitialOffer>>();

                foreach (var offer in initialOffer)
                {
                    offer.OfferId = Guid.NewGuid().ToString("N");
                }

                initialOfferResult.Offers = initialOffer.ConvertAll(o => (IInitialOffer)o);
                initialOfferResult.Status = ScoreCardApplicationStatus.Completed;
                initialOfferResult.GeneratedOn = new TimeBucket(TenantTime.Now);

                if (ApplicationProcessorConfiguration.EnableEmail)
                {
                    try
                    {
                        CallEmailVerificationSyndication(applicationNumber, applicantInformation.PersonalEmail, applicantInformation.FirstName);
                    }
                    catch (Exception ex)
                    {
                        Logger.Error(ex.Message);
                    }
                }
                try
                {
                    CallViewDnsSyndication(applicationNumber, applicantInformation.CompanyEmailAddress, applicantInformation.CompanyName, applicantInformation.CinNumber, isReCallSyndication);
                }
                catch (Exception ex)
                {
                    Logger.Error(ex.Message);
                }
                initialOfferResult.IsAvailable = true;
            }
            catch (OfferDeclinedException exception)
            {
                initialOfferResult.Status = ScoreCardApplicationStatus.Rejected;
                initialOfferResult.Reasons = exception.ScoreCardResult.ResultDetail;
            }
            catch (OfferGenerationFailedException exception)
            {
                initialOfferResult.Status = ScoreCardApplicationStatus.Failed;
                initialOfferResult.Reasons = new List<string> { exception.Message };
            }
            catch (AggregateException exception)
            {
                initialOfferResult.Status = ScoreCardApplicationStatus.Failed;
                initialOfferResult.Reasons = new List<string>() { exception.InnerException.Message };
            }
            catch (Exception exception)
            {
                initialOfferResult.Status = ScoreCardApplicationStatus.Failed;
                initialOfferResult.Reasons = new List<string>() { exception.Message };
            }
            
            await OfferEngineRepository.AddOffer("application", applicationNumber, initialOfferResult);
            Logger.Info($"[OrchestrationService] RunInitialOffer method  execution finished for [{applicationNumber}]");
            return initialOfferResult;
        }


        #region Pan Verification

        //private IPanVerificationResponse CallPanVerificationSyndication(string applicationNumber,
        //    IApplicantInformation applicantInformation)
        //{
        //    var objPanVerification = new PanVerificationRequest
        //    {
        //        Dob = applicantInformation.DateOfBirth.LocalDateTime.ToString(),
        //        Gender =
        //            applicantInformation.Gender == Gender.Male
        //                ? Syndication.Crif.Gender.Male.ToString()
        //                : Syndication.Crif.Gender.Female.ToString(),
        //        Name = applicantInformation.FirstName,
        //        Pan = applicantInformation.Pan
        //    };

        //    IPanVerificationResponse panVerificationResponse = null;

        //    try
        //    {
        //        FaultRetry.RunWithAlwaysRetry(() =>
        //        {
        //            panVerificationResponse =
        //                CrifPanVerificationService.VerifyPan("application", applicationNumber, objPanVerification).Result;
        //        });
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.Error(ex.Message + " Stack Trace: " + ex.StackTrace);
        //    }
        //    return panVerificationResponse;
        //}

        //private void VerifyPan(string applicationNumber, IApplicantInformation applicantInformation, bool isReCallSyndication)
        //{
        //    var count = 1;
        //    const int maxAttempt = 3;
        //    dynamic attributes = null;
        //    try
        //    {
        //        attributes = DataAttributesEngine.GetAttribute("application", applicationNumber, "cibilReport").Result;
        //        if (attributes != null)
        //        {
        //            var panVerificationStatus = VerificationEngineService.GetStatus("application", applicationNumber, "PanVerification").Result;

        //            if (panVerificationStatus.VerificationStatus == VerificationStatus.Failed && (string.IsNullOrEmpty(JObject.Parse(attributes.ToString()).panNameMatch.Value) || string.IsNullOrEmpty(JObject.Parse(attributes.ToString()).panNoMatch.Value)))
        //            {
        //                VerificationEngineService.InitiateManualVerification("application", applicationNumber, "PanVerification").Wait();
        //                return;
        //            }
        //        }
        //    }
        //    catch(Exception ex)
        //    {
        //        Logger.Error("Error Occured while execute" + ex.StackTrace);
        //    }
        //    FaultRetry.RunWithAlwaysRetry(() =>
        //    {
        //        var panVerificationStatus = VerificationEngineService.GetStatus("application", applicationNumber, "PanVerification").Result;
        //        if (panVerificationStatus == null)
        //        {
        //            if (count < maxAttempt)
        //            {
        //                count++;
        //                throw new Exception("Pan verification Failed");
        //            }
        //        }               
        //    });
        //}

        //Dictionary<string, object> attributes = null;
        //if (!isReCallSyndication)
        //{
        //    attributes = DataAttributesEngine.GetAttribute("application", applicationNumber,
        //      new List<string> { "crifPanReport" }).Result;
        //}
        //Dictionary<string, string> referenceNumbers = null;
        //if (attributes == null || attributes.Count <= 0)
        //{
        //    var syndicationResult = CallPanVerificationSyndication(applicationNumber, applicantInformation);

        //    if (syndicationResult == null)
        //        throw new OfferGenerationFailedException("Unable to get syndication response for PanVerification");
        //    referenceNumbers = new Dictionary<string, string>
        //{
        //    {"crifPanReport", syndicationResult.ReferenceNumber}
        //};
        //}

        //VerifyRule(applicationNumber, "EligibilityCriteria", "Prequalification", "CrifPanVerificationRule",
        //    referenceNumbers);

        #endregion

        #region Crif Credit Report

        private string CallCreditReportSyndication(string applicationNumber,
            IApplicantInformation applicantInformation, string bureauType, bool isReCallSyndication)
        {
            Logger.Info($"[OrchestrationService/RunInitialOffer] CallCreditReportSyndication method execution started for [{applicationNumber}] ");
            string referenceNumber = string.Empty;
            object attributes = null;
            if (bureauType == "CIBIL")
            {
                if (!isReCallSyndication)
                {

                    attributes = DataAttributesEngine.GetAttribute("application", applicationNumber, "cibilReport").Result;

                }

                if (attributes == null)
                {
                    var cibilReport = CallCibilCreditReportSyndication(applicationNumber, applicantInformation);

                    //if (cibilReport != null)
                    //{
                    //    string cibilFileType = null;
                    //    FaultRetry.RunWithAlwaysRetry(() =>
                    //    {

                    //        attributes = DataAttributesEngine.GetAttribute("application", applicationNumber, "cibilReport").Result;

                    //        if (attributes != null)
                    //            cibilFileType = GetFileTypeValue(attributes);
                    //    });
                    //if (cibilFileType != BureauFileType.Thick.ToString() && cibilFileType != BureauFileType.Thin.ToString())
                    //{
                    //    CallPanVerificationSyndication(applicationNumber, applicantInformation);
                    //    var syndicationResult = CallCrifCreditReportSyndication(applicationNumber, applicantInformation);
                    //    if (syndicationResult != null)
                    //        referenceNumber = syndicationResult.ReferenceNumber;
                    //}
                    //}
                    //else
                    //{
                    //    CallPanVerificationSyndication(applicationNumber, applicantInformation);
                    //    var syndicationResult = CallCrifCreditReportSyndication(applicationNumber, applicantInformation);
                    //    if (syndicationResult != null)
                    //        referenceNumber = syndicationResult.ReferenceNumber;
                    //}
                }
            }
            //else if (bureauType == "CRIF")
            //{
            //    object crifAttributes = null;
            //    if (!isReCallSyndication)
            //    {
            //        crifAttributes = DataAttributesEngine.GetAttribute("application", applicationNumber, "crifPanReport").Result;
            //    }
            //    Dictionary<string, string> referenceNumbers = null;
            //    if (crifAttributes == null)
            //    {
            //        var syndicationResult = CallPanVerificationSyndication(applicationNumber, applicantInformation);

            //        if (syndicationResult == null)
            //            throw new OfferGenerationFailedException("Unable to get syndication response for PanVerification");

            //        referenceNumbers = new Dictionary<string, string>
            //    {
            //        {"crifPanReport", syndicationResult.ReferenceNumber}
            //    };
            //    }
            //    string crifFileType = null;
            //    if (!isReCallSyndication)
            //    {
            //        attributes = DataAttributesEngine.GetAttribute("application", applicationNumber, "crifCreditReport").Result;

            //    }

            //    if (attributes == null)
            //    {
            //        var result = CallCrifCreditReportSyndication(applicationNumber, applicantInformation);
            //        if (result != null)
            //        {
            //            referenceNumber = result.ReferenceNumber;
            //            FaultRetry.RunWithAlwaysRetry(() =>
            //            {
            //                attributes = DataAttributesEngine.GetAttribute("application", applicationNumber, "crifCreditReport").Result;
            //                if (attributes != null)
            //                    crifFileType = GetFileTypeValue(attributes);
            //            });
            //            if (crifFileType != BureauFileType.Thick.ToString() && crifFileType != BureauFileType.Thin.ToString())
            //            {
            //                var syndicationResult = CallCibilCreditReportSyndication(applicationNumber, applicantInformation);
            //            }
            //        }
            //        else
            //        {
            //            var syndicationResult = CallCibilCreditReportSyndication(applicationNumber, applicantInformation);
            //        }

            //    }
            //}
            Logger.Info($"[OfferEngineService/RunInitialOffer] CallCreditReportSyndication method execution finished for [{applicationNumber}] ");
            return referenceNumber;
        }

        private IReport CallCibilCreditReportSyndication(string applicationNumber, IApplicantInformation applicantInformation)
        {
            Logger.Info($"[OrchestrationService/RunInitialOffer/CallCreditReportSyndication] CallCibilCreditReportSyndication method execution started for [{applicationNumber}] payload :[{  (applicantInformation != null ? JsonConvert.SerializeObject(applicantInformation) : null)}] ");
            IReportRequest cibilRequest = new ReportRequest();
            CultureInfo cultureInfo = Thread.CurrentThread.CurrentCulture;
            TextInfo textInfo = cultureInfo.TextInfo;

            foreach (var address in applicantInformation.Addresses)
            {

                if (address.AddressType.ToString() == "Current")
                {
                    cibilRequest.Address1ResidentOccupation = applicantInformation.ResidenceType.ToString();
                    cibilRequest.Address1Line1 = address.AddressLine1;
                    cibilRequest.Address1Line2 = address.AddressLine2;
                    cibilRequest.Address1Line3 = address.AddressLine3;
                    cibilRequest.Address1Pincode = address.PinCode;
                    cibilRequest.Address1State = address.State.ToString();
                    cibilRequest.Address1City = address.City;
                    cibilRequest.Address1AddressCategory = address.AddressType.ToString();
                }
            }

            if (applicantInformation.employmentAddress != null && applicantInformation.employmentAddress.Count > 0)
            {
                cibilRequest.Address2Line1 = applicantInformation.employmentAddress[0].AddressLine1;
                cibilRequest.Address2Line2 = applicantInformation.employmentAddress[0].AddressLine2;
                cibilRequest.Address2Line3 = applicantInformation.employmentAddress[0].AddressLine3;
                cibilRequest.Address2Pincode = applicantInformation.employmentAddress[0].PinCode;
                cibilRequest.Address2State = applicantInformation.employmentAddress[0].State;
                cibilRequest.Address2City = applicantInformation.employmentAddress[0].City;

            }
            cibilRequest.AdharNumber = applicantInformation.AadhaarNumber;

            cibilRequest.ApplicantFirstName = applicantInformation.FirstName;
            cibilRequest.ApplicantLastName = applicantInformation.LastName;
            cibilRequest.ApplicantMiddleName = applicantInformation.MiddleName;
            cibilRequest.DateOfBirth = applicantInformation.DateOfBirth.LocalDateTime;
            cibilRequest.EmailId = applicantInformation.PersonalEmail;

            cibilRequest.Gender = applicantInformation.Gender.ToString();

            cibilRequest.LandLineNo = applicantInformation.LandlineNo;

            cibilRequest.MobileNo = applicantInformation.MobileNo;

            cibilRequest.LoanAmount = applicantInformation.RequestedAmount.ToString();
            cibilRequest.OfficeLandLineNo = "";
            cibilRequest.PanNo = applicantInformation.Pan;
            cibilRequest.AdharNumber = applicantInformation.AadhaarNumber;
            cibilRequest.PassportNumber = "";
            cibilRequest.RationCardNo = "";
            cibilRequest.VoterId = "";

            IReport result = null;

            try
            {
                Logger.Info($"[OrchestrationService/RunInitialOffer/CallCreditReportSyndication/CallCibilCreditReportSyndication]  CibilReportService GetReport method  execution started for [{applicationNumber}] cibilrequest :[{  (cibilRequest != null ? JsonConvert.SerializeObject(cibilRequest) : null)}] ");
                FaultRetry.RunWithAlwaysRetry(() =>
                {
                    CibilReportService.GetReport("application", applicationNumber, cibilRequest).Wait();

                });
                Logger.Info($"[OrchestrationService/RunInitialOffer/CallCreditReportSyndication/CallCibilCreditReportSyndication]  CibilReportService GetReport method  execution finished for [{applicationNumber}] result :[{  (result != null ? JsonConvert.SerializeObject(result) : null)}] ");
            }
            catch (Exception ex)
            {
                result = null;
                Logger.Error(ex.Message + " Stack Trace:" + ex.StackTrace);
            }
            Logger.Info($"[OrchestrationService/RunInitialOffer/CallCreditReportSyndication] CallCibilCreditReportSyndication method execution finished for [{applicationNumber}] result :[{  (result != null ? JsonConvert.SerializeObject(result) : null)}] ");
            return result;
        }
        
        //private IGetCreditReportResponse CallCrifCreditReportSyndication(string applicationNumber, IApplicantInformation applicantInformation)
        //{

        //    ICreditReportInquiryRequest request = new CreditReportInquiryRequest();

        //    request.Addresses = new List<IAddressSegment>(new List<AddressSegment>());

        //    foreach (var address in applicantInformation.Addresses)
        //    {
        //        Syndication.Crif.State addState;
        //        Enum.TryParse(address.State, out addState);
        //        AddressType addType;
        //        Enum.TryParse(address.AddressType.ToString(), out addType);
        //        request.Addresses.Add(new AddressSegment()
        //        {
        //            Address =
        //                address.AddressLine1 + address.AddressLine2 + address.AddressLine3 +
        //                address.AddressLine4,
        //            City = address.City,
        //            Pin = address.PinCode,
        //            State = addState,
        //            Type = addType
        //        });
        //    }

        //    request.Dob = applicantInformation.DateOfBirth.LocalDateTime.ToString();
        //    request.Gender = applicantInformation.Gender == Gender.Male
        //        ? Syndication.Crif.Gender.Male
        //        : Syndication.Crif.Gender.Female;

        //    request.LandlineNo = applicantInformation.LandlineNo;

        //    request.MobileNo = applicantInformation.MobileNo;

        //    request.Name = applicantInformation.FirstName + " " + applicantInformation.MiddleName + " " +
        //                   applicantInformation.LastName;
        //    request.Pan = applicantInformation.Pan;

        //    IGetCreditReportResponse result = null;

        //    try
        //    {
        //        FaultRetry.RunWithAlwaysRetry(() =>
        //    {
        //        result = CrifCreditReportService.GetReport("application", applicationNumber, request).Result;
        //    });
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.Error(ex.Message + "Stack Trace:" + ex.StackTrace);
        //    }

        //    return result;
        //}

        private void VerifyCreditReportEligibility(string applicationNumber)
        {
            //Dictionary<string, string> referenceNumbers = new Dictionary<string, string>();
            //if (!string.IsNullOrEmpty(referenceNumber))
            //{
            //    referenceNumbers.Add("crifCreditReport", referenceNumber);
            //};
            Logger.Info($"[OrchestrationService/RunInitialOffer] VerifyCreditReportEligibility method execution started for [{applicationNumber}]");
            VerifyRule(applicationNumber, "EligibilityCriteria",
                "Prequalification", "CreditEligibilityCheck", null);
            Logger.Info($"[OrchestrationService/RunInitialOffer] VerifyCreditReportEligibility method execution finshed for [{applicationNumber}]");
        }

        #endregion

        #region Geo Profile

        private string CallGeoProfileSyndication(string applicationNumber, string pinCode, bool isReCallSyndication)
        {
            Logger.Info($"[OrchestrationService/RunInitialOffer] CallGeoProfileSyndication method execution started for [{applicationNumber}] pincode :[{pinCode}]");

            string referenceNumber = string.Empty;
            object attributes = null;
            if (!isReCallSyndication)
            {
                attributes = DataAttributesEngine.GetAttribute("application", applicationNumber, "geoRiskPinCode").Result;
            }
            if (attributes == null)
            {
                GeoPincodeResponse result = null;
                Logger.Info($"[OrchestrationService/RunInitialOffer/CallGeoProfileSyndication]  GeoProfileService GetPincode method execution started for [{applicationNumber}] pincode :[{pinCode}]");
                FaultRetry.RunWithAlwaysRetry(() =>
                {
                    result = GeoProfileService.GetPincode("application", applicationNumber, pinCode).Result;
                });
                Logger.Info($"[OrchestrationService/RunInitialOffer/CallGeoProfileSyndication] GeoProfileService GetPincode method execution finished for [{applicationNumber}] pincode :[{pinCode}]");
                if (result != null)
                    referenceNumber = result.ReferenceNumber;
            }
            Logger.Info($"[OrchestrationService/RunInitialOffer] CallGeoProfileSyndication method execution finished for [{applicationNumber}] ");
            return referenceNumber;
        }

        private void VerifyGeoProfile(string applicationNumber, Dictionary<string, string> referenceNumbers)
        {
            //var referenceNumbers = new Dictionary<string, string>
            //{
            //    {"geoRiskPinCode", geoPinCodeSyndicationResult.ReferenceNumber},
            //    {"crifCreditReport", creditReportSyndicationResponse.ReferenceNumber}
            //};
            Logger.Info($"[OrchestrationService/RunInitialOffer] VerifyGeoProfile method execution started for [{applicationNumber}]");
            VerifyRule(applicationNumber, "EligibilityCriteria",
                "Prequalification", "GeoRiskEligibility", referenceNumbers);
            Logger.Info($"[OrchestrationService/RunInitialOffer] VerifyGeoProfile method execution finished for [{applicationNumber}]");
        }

        #endregion

        private async Task UpdateBankInformation(string applicationNumber, IBankInformation bankInformationRequest)
        {

            bankInformationRequest.AccountType = AccountType.Savings;
            var status = await EntityStatusService.GetStatusByEntity("application", applicationNumber);
            var application = await ExternalApplicationService.GetByApplicationNumber(applicationNumber);

            if (application == null)
                throw new NotFoundException($"Application with number {applicationNumber} is not found");

            if (application != null && status != null && status.Code == ApplicationProcessorConfiguration.Statuses["Approved"])
            {
                if (string.IsNullOrWhiteSpace(bankInformationRequest.BankId))
                {
                    await ApplicantService.AddBank(application.ApplicantId, bankInformationRequest);
                }
                else
                {
                    await ApplicantService.UpdateBank(application.ApplicantId, bankInformationRequest.BankId, bankInformationRequest);
                }
            }
            else
            {
                if (status != null && ApplicationProcessorConfiguration != null && !(ApplicationProcessorConfiguration.BankChangeStatus.Contains(status.Code)))
                {
                    throw new InvalidOperationException($"Can not update bank detail when status is {status.Code}");
                }
                await ExternalApplicationService.UpdateBankInformation(applicationNumber, bankInformationRequest);
            }
        }
        private async Task<string> CallPerfiosCreditReportSyndication(string applicationNumber, bool isReCallSyndication)
        {
            string referenceNumber = string.Empty;
            object attributes = null;
            if (!isReCallSyndication)
            {
                attributes = DataAttributesEngine.GetAttribute("application", applicationNumber, "perfiosReport").Result;

            }
            if (attributes == null)
            {

                IAnalysisReport result = null;

                FaultRetry.RunWithAlwaysRetry(() =>
               {
                   try
                   {
                       var perfiosdata = PerfiosService.RetrieveXmlReport("application", applicationNumber).Result;
                       result = (AnalysisReport)perfiosdata;


                   }
                   catch (Exception ex)
                   {
                       Logger.Error(ex.Message);
                   }
               });
                if (result != null)
                {
                    try
                    {
                        referenceNumber = result.ReferenceNumber;
                        if (result?.CustomerInfo?.SourceOfData == "NetBanking Account")
                        {
                            await UpdateBankInformation(applicationNumber, new BankInformation()
                            {
                                AccountHolderName = result.CustomerInfo.Name,
                                AccountType = AccountType.Savings,
                                IfscCode = result.IFSCCode,
                                AccountNumber = result.AccountNo,
                                BankName = result.Bank,
                                BankAddresses = new Address()
                                {
                                    AddressLine1 = result.Location,
                                    AddressType = Applicant.AddressType.Bank
                                }
                            });
                            Logger.Info("Bank detail update for netbanking");
                            await VerificationEngineService.Verify("application", applicationNumber, "BankVerification",
                           "PerfiosNetBanking", "PerfiosNetBanking", new { result = "true" });
                            Logger.Info("Automatic fact verification done for netbanking");
                        }
                    }
                    catch (Exception ex)
                    {
                        Logger.Error(ex.Message);
                    }
                }


            }
            return referenceNumber;
        }
        // if perfios verification failed by syndication then we will initiate manual for the same
        private void VerifyPerfiosInformation(string applicationNumber)
        {
            var bankVerificationStatus = VerificationEngineService.GetStatus("application", applicationNumber, "BankVerification").Result;


            if (bankVerificationStatus != null && bankVerificationStatus.VerificationStatus != VerificationStatus.Passed)
            {
                VerificationEngineService.InitiateManualVerification("application", applicationNumber, "BankVerification", "BankManual").Wait();
            }
        }

        private string CallCompanyDbSyndication(string applicationNumber, string cinNumber, bool isReCallSyndication)
        {
            Logger.Info($"[OrchestrationService/RunInitialOffer] CallCompanyDbSyndication method execution started for [{applicationNumber}] Cin :[{cinNumber}]");
            string referenceNumber = string.Empty;
            object attributes = null;
            if (!isReCallSyndication)
            {
                attributes = DataAttributesEngine.GetAttribute("application", applicationNumber, "companyCategoryReport").Result;
            }
            if (attributes == null)
            {
                RuleResult result = null;
                FaultRetry.RunWithAlwaysRetry(() =>
                {
                    var companyDirectors = CompanyService.GetCompanyWithDirectors("application", applicationNumber, cinNumber).Result;
                    result = CompanyService.GetCompanyCategory("application", applicationNumber, companyDirectors, cinNumber).Result;
                });
                if (result != null)
                    referenceNumber = result.ReferenceNumber;
            }
            Logger.Info($"[OrchestrationService/RunInitialOffer] CallCompanyDbSyndication method execution finished for [{applicationNumber}] Cin :[{cinNumber}]");
            return referenceNumber;
        }

        private IGetApplicationScoreResponse CallLenddoSyndication(string applicationNumber)
        {
            string referenceNumber = string.Empty;
            object attributes = null;

            attributes = DataAttributesEngine.GetAttribute("application", applicationNumber, "clientScoreCard").Result;
            IGetApplicationScoreResponse result = null;
            if (attributes == null)
            {
                FaultRetry.RunWithAlwaysRetry(() =>
            {
                result = LenddoService.GetClientScore("application", applicationNumber, applicationNumber).Result;
            });
            }
            return result;
        }

        private void CallViewDnsSyndication(string applicationNumber, string emailAddress, string companyName, string cinNumber, bool isReCallSyndication)
        {
            Logger.Info($"[OrchestrationService/RunInitialOffer] CallViewDnsSyndication method execution started for [{applicationNumber}] email :[{emailAddress}] companyName:[{companyName}]  cinNumber:[{cinNumber}] ");
            string referenceNumber = string.Empty;
            Dictionary<string, object> attributes = null;
            if (!isReCallSyndication)
            {
                //TODO : No data gourp is defined for this in data-attributes.
                //attributes = DataAttributesEngine.GetAttribute("application", applicationNumber,
                //new List<string> { "companyCategoryReport" }).Result;
            }
            if (attributes == null || attributes.Count <= 0)
            {
                FaultRetry.RunWithAlwaysRetry(() =>
            {
                ViewDnsService.VerifyEmployment("application", applicationNumber, emailAddress, companyName, cinNumber).Wait();
            });
            }
            Logger.Info($"[OrchestrationService/RunInitialOffer] CallViewDnsSyndication method execution finished for [{applicationNumber}] email :[{emailAddress}] companyName:[{companyName}]  cinNumber:[{cinNumber}] ");
        }

        private void CallEmailVerificationSyndication(string applicationNumber, string emailAddress, string name)
        {
            Logger.Info($"[OrchestrationService/RunInitialOffer] CallEmailVerificationSyndication method execution started for [{applicationNumber}] email :[{emailAddress}] name:[{name}] ");
            TextInfo textInfo = new CultureInfo("en-US", false).TextInfo;
            EmailVerificationRequest request = new EmailVerificationRequest
            {
                Email = emailAddress,
                Source = "borrower",
                Name = textInfo.ToTitleCase(name ?? string.Empty),
                Data = null
            };
            FaultRetry.RunWithAlwaysRetry(() =>
            {
                EmailVerificationService.InitiateEmailVerification("application", applicationNumber, request).Wait();
            });
            Logger.Info($"[OrchestrationService/RunInitialOffer] CallEmailVerificationSyndication method execution finished for [{applicationNumber}] email :[{emailAddress}] name:[{name}] ");
        }
        #region Private Method

        private IScoreCardRuleResult VerifyRule(string applicationNumber, string ruleType, string group,
            string rule, Dictionary<string, string> referenceNumbers)
        {
            Logger.Info($"[OrchestrationService] VerifyRule method execution started for [{applicationNumber}] rule : [{rule}]");
            ScoreCardRuleResult ruleExecutionResult = null;
            var count = 1;
            const int maxAttempt = 3;

            FaultRetry.RunWithAlwaysRetry(() =>
            {
                var result =
                    ScoreCardService.RunRule("application", applicationNumber, "Product1", ruleType, group, rule, referenceNumbers).Result;

                if (result == null || !result.Any())
                    throw new OfferGenerationFailedException("Unable to execute rule : " + rule);

                ruleExecutionResult = result.FirstOrDefault();
                Logger.Info($"[OrchestrationService] VerifyRule method execution intermediate result for [{applicationNumber}] rule :  [{rule}] result : [{  (ruleExecutionResult != null ? JsonConvert.SerializeObject(ruleExecutionResult) : null)}]");
                if (ruleExecutionResult == null || ruleExecutionResult.Result == false)
                {
                    if (count < maxAttempt)
                    {
                        count++;
                        if (ruleExecutionResult != null && ruleExecutionResult.Result == false)
                            throw new OfferDeclinedException(ruleExecutionResult);

                        throw new OfferGenerationFailedException("Unable to execute rule");
                    }
                }
            });

            if (ruleExecutionResult == null)
                throw new OfferGenerationFailedException("Unable to execute rule");

            if (ruleExecutionResult.Result == false)
            {
                if (ruleExecutionResult.ExceptionDetail != null && ruleExecutionResult.ExceptionDetail.Count > 0)
                {
                    throw new OfferGenerationFailedException("Unable to generate offer");
                }
                else if (ruleExecutionResult.ResultDetail != null && ruleExecutionResult.ResultDetail.Count > 0)
                {
                    throw new OfferDeclinedException(ruleExecutionResult);
                }
            }
            Logger.Info($"[OrchestrationService] VerifyRule method execution finished for [{applicationNumber}] result : [{  (ruleExecutionResult != null ? JsonConvert.SerializeObject(ruleExecutionResult) : null)}]");
            return ruleExecutionResult;
        }

        private async Task<IApplicantInformation> GetApplicationInformation(string applicationNumber)
        {
            var attributes = await DataAttributesEngine.GetAttribute("application", applicationNumber,
                new List<string> { "application" });

            if (attributes == null || !attributes.Any())
                throw new OfferGenerationFailedException("No data attributes found for application source.");

            object applicationAttribute;
            if (!attributes.TryGetValue("application", out applicationAttribute))
                throw new OfferGenerationFailedException("application data attributes not found");

            var applicantInformation = JObject.FromObject(applicationAttribute).ToObject<ApplicantInformation>();
            return applicantInformation;
        }

        private static string GetFileTypeValue(dynamic data)
        {
            Func<dynamic> resultProperty = () => data.fileType;

            return HasProperty(resultProperty) ? GetValue(resultProperty) : string.Empty;
        }
        private static bool HasProperty<T>(Func<T> property)
        {
            try
            {
                property();
                return true;
            }
            catch (RuntimeBinderException ex)
            {
                return false;
            }
        }

        private static T GetValue<T>(Func<T> property)
        {
            return property();
        }
        #endregion
    }
}
