﻿using System;
using System.Threading.Tasks;
using CreditExchange.Application;
using LendFoundry.Consent;
using LendFoundry.DataAttributes;
using LendFoundry.StatusManagement;
// using CreditExchange.Syndication.EmailHunter;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using CreditExchange.Applicant;
using CreditExchange.ApplicationProcessor.Events;
using CreditExchange.Applications.Filters.Abstractions;
using CreditExchange.Applications.Filters.Abstractions.Services;
using CreditExchange.Perfios;
using CreditExchange.Syndication.Perfios.Response;
using LendFoundry.Application.Document;
using LendFoundry.Clients.DecisionEngine;
using LendFoundry.EventHub;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Lookup;
using LendFoundry.Foundation.Services;
using LendFoundry.VerificationEngine;
using Microsoft.CSharp.RuntimeBinder;
// using CreditExchange.Syndication.TowerData;
using System.Globalization;
using CreditExchange.ScoreCard;
using LendFoundry.AlertEngine;
using LendFoundry.EmailVerification;
using LendFoundry.Foundation.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;

namespace CreditExchange.ApplicationProcessor
{
    public class ApplicationService : IApplicationService
    {

        public ApplicationService (IConsentService consentService, IOfferEngineService offerEngineService, IDataAttributesEngine dataAttributesEngine, Application.IApplicationService applicationService, //IEmailHunterService emailHunterService,
            IDecisionEngineService decisionEngineService, IEntityStatusService entityStatusService, IVerificationEngineService verificationEngineService, ITenantTime tenantTime, ApplicationProcessorConfiguration applicationProcessorConfiguration,
            ILookupService lookupService, IApplicationFilterService applicationFilterService, IPerfiosService perfiosService, IApplicationDocumentService applicationDocumentService, IEventHubClient eventHub, ILogger logger, IApplicantService applicantService,
            //ITowerDataService towerDataService, 
            IAlertEngineService alertEngineService, IEmailVerificationService emailVerificationService, IScoreCardService scoreCardService)
        {
            ConsentService = consentService;
            //  OfferEngineService = offerEngineService;
            DataAttributesEngine = dataAttributesEngine;
            ExternalApplicationService = applicationService;
            TenantTime = tenantTime;
            //EmailHunterService = emailHunterService;
            DecisionEngineService = decisionEngineService;
            EntityStatusService = entityStatusService;
            VerificationEngineService = verificationEngineService;
            ApplicationProcessorConfiguration = applicationProcessorConfiguration;
            LookupService = lookupService;
            ApplicationFilterService = applicationFilterService;
            ApplicationDocumentService = applicationDocumentService;
            PerfiosService = perfiosService;
            EventHub = eventHub;
            Logger = logger;
            ApplicantService = applicantService;
            //TowerDataService = towerDataService;
            AlertEngineService = alertEngineService;
            EmailVerificationService = emailVerificationService;
            ScoreCardService = scoreCardService;
        }
        private IEventHubClient EventHub { get; }
        private IApplicationDocumentService ApplicationDocumentService { get; }
        private IPerfiosService PerfiosService { get; }
        private ILookupService LookupService { get; }
        private ApplicationProcessorConfiguration ApplicationProcessorConfiguration { get; }

        private IConsentService ConsentService { get; }

        private IOfferEngineService OfferEngineService { get; }

        private IDataAttributesEngine DataAttributesEngine { get; }

        private IEntityStatusService EntityStatusService { get; }

        // private IEmailHunterService EmailHunterService { get; }

        private IDecisionEngineService DecisionEngineService { get; }

        private IVerificationEngineService VerificationEngineService { get; }

        private Application.IApplicationService ExternalApplicationService { get; }
        private IEmailVerificationService EmailVerificationService { get; }

        private ITenantTime TenantTime { get; }

        private IApplicationFilterService ApplicationFilterService { get; }

        private ILogger Logger { get; }

        private IApplicantService ApplicantService { get; }
        //private ITowerDataService TowerDataService { get; }

        private IAlertEngineService AlertEngineService { get; }
        private IScoreCardService ScoreCardService { get; }

        //public async Task<IConsent> SignLoanAggrement(string applicationNumber)
        //{
        //    var attributes = await DataAttributesEngine.GetAllAttributes("application", applicationNumber);
        //    var finalOffer = await OfferEngineService.GetFinalOffer(applicationNumber);
        //    var dateOfEmi = TenantTime.Now;
        //    if (TenantTime.Now.Day < 20)
        //    {
        //        dateOfEmi = TenantTime.Now.AddMonths(1);
        //        dateOfEmi = TenantTime.Create(dateOfEmi.Year, dateOfEmi.Month, 5);
        //    }
        //    else
        //    {
        //        dateOfEmi = TenantTime.Create(dateOfEmi.Year, dateOfEmi.Month, 5);
        //    }
        //    var body = new { IPAddress = "10.1.1.99", SignedBy = "1", dataAttribute = attributes, offerSelected = finalOffer, LoanDate = TenantTime.Now, DateofEMI = dateOfEmi, AdvanceEMI = 0, AdvanceEMIAmount = 0 };

        //    return await ConsentService.Sign("application", applicationNumber, "loanAgreement", body);
        //}

        public async Task<Tuple<IApplicationResponse, string>> SubmitApplication (ISubmitApplicationRequest submitApplicationRequest)
        {
            string reason;
            var isValid = EnsureInputIsValid (submitApplicationRequest, out reason);

            var isExists = false;

            if (!string.IsNullOrWhiteSpace (submitApplicationRequest.PersonalMobile))
            {
                isExists = await CheckIfDuplicateApplicationExists (UniqueParameterTypes.Mobile, submitApplicationRequest.PersonalMobile, string.Empty);
                if (isExists)
                    throw new InvalidOperationException ("Application with same mobile number already exists");
            }

            if (!string.IsNullOrWhiteSpace (submitApplicationRequest.PermanentAccountNumber))
            {
                isExists = await CheckIfDuplicateApplicationExists (UniqueParameterTypes.Pan, submitApplicationRequest.PermanentAccountNumber, string.Empty);
                if (isExists)
                    throw new InvalidOperationException ("Application with same permanent account number already exists");
            }

            if (!string.IsNullOrWhiteSpace (submitApplicationRequest.PersonalEmail))
            {
                isExists = await CheckIfDuplicateApplicationExists (UniqueParameterTypes.Email, submitApplicationRequest.PersonalEmail, string.Empty);
                if (isExists)
                    throw new InvalidOperationException ("Application with same email id already exists");
            }

            var applicationRequest = GetApplicationRequest (submitApplicationRequest);
            // Call Email Hunter to verify the email Address

            // Application Submitted 
            var application = await ExternalApplicationService.Add (applicationRequest).ConfigureAwait (false);
            if (application == null)
            {
                throw new Exception ("There is some issue in creating application");
            }
            await SwitchOffReApply (application.ApplicationNumber, application.ApplicantId);

            string emailVerifyResponse = "true";
            // if (ApplicationProcessorConfiguration.EnablePersonalEmailVerification)
            // {
            //     if (!string.IsNullOrWhiteSpace(ApplicationProcessorConfiguration.EmailValidationProvider) && ApplicationProcessorConfiguration.EmailValidationProvider.ToLower() == "emailhunter")
            //     {
            //         emailVerifyResponse = VerifyEmailAddressWithHunter(application.ApplicationNumber, submitApplicationRequest.PersonalEmail);
            //     }
            //     else if (!string.IsNullOrWhiteSpace(ApplicationProcessorConfiguration.EmailValidationProvider) && ApplicationProcessorConfiguration.EmailValidationProvider.ToLower() == "towerdata")
            //     {
            //         //emailVerifyResponse = VerifyEmailAddressWithTowerData(application.ApplicationNumber, submitApplicationRequest.PersonalEmail);
            //     }
            // }
            await VerificationEngineService.Verify ("application", application.ApplicationNumber, "MobileVerification",
                "MobileSMS", "MobileSMS", new { result = submitApplicationRequest.IsMobileVerified.ToString ().ToLower () });

            //await VerificationEngineService.Verify("application", application.ApplicationNumber, "EmailVerifyForPersonal",
            //   "AutomaticPersonalEmail", "PersonalEmail", new { result = emailVerifyResponse.ToLower() });

            string status;
            // change status to application submitted 
            if (isValid == false || submitApplicationRequest.IsMobileVerified.ToString ().ToLower () != "true")
            {
                await EntityStatusService.ChangeStatus ("application", application.ApplicationNumber, ApplicationProcessorConfiguration.Statuses["LeadCreated"], null);
                status = "LeadCreated";
                await EventHub.Publish (new ApplicationCreatedWithLeadStatus { ApplicationNumber = application.ApplicationNumber, Status = status, Reason = reason });
            }
            else
            {
                await EntityStatusService.ChangeStatus ("application", application.ApplicationNumber, ApplicationProcessorConfiguration.Statuses["ApplicationSubmitted"], null);
                status = "ApplicationSubmitted";
            }

            await DataAttributesEngine.SetAttribute ("application", application.ApplicationNumber, "extension", GetApplicationExtension (submitApplicationRequest, emailVerifyResponse, ApplicationProcessorConfiguration.EnablePersonalEmailVerification, ApplicationProcessorConfiguration.EmailValidationProvider.ToLower ()));
            return new Tuple<IApplicationResponse, string> (application, status);
        }

        public async Task<Tuple<IApplication, string>> UpdateApplication (string applicationNumber, ISubmitApplicationRequest submitApplicationRequest)
        {
            Logger.Info ($"[OfferEngine/ApplicationService] UpdateApplication method execution started for [{applicationNumber}] request : [{  (submitApplicationRequest != null ? JsonConvert.SerializeObject(submitApplicationRequest) : null)}] ");
            var activeWorkflow = await EntityStatusService.GetActiveStatusWorkFlow ("application", applicationNumber);
            var status = EntityStatusService.GetStatusByEntity ("application", applicationNumber, activeWorkflow.StatusWorkFlowId).Result;
            if (ApplicationProcessorConfiguration.Statuses["LeadCreated"] != status.Code)
            {
                throw new InvalidOperationException ($"Can not update application when status is {status.Code}");
            }
            string reason;
            var isValid = EnsureInputIsValid (submitApplicationRequest, out reason);
            var isExists = false;

            if (!string.IsNullOrWhiteSpace (submitApplicationRequest.PersonalMobile))
            {
                isExists = await CheckIfDuplicateApplicationExists (UniqueParameterTypes.Mobile, submitApplicationRequest.PersonalMobile, applicationNumber).ConfigureAwait (false);
                if (isExists)
                    throw new InvalidOperationException ("Application with same mobile number already exists");
            }

            if (!string.IsNullOrWhiteSpace (submitApplicationRequest.PermanentAccountNumber))
            {
                isExists = await CheckIfDuplicateApplicationExists (UniqueParameterTypes.Pan, submitApplicationRequest.PermanentAccountNumber, applicationNumber).ConfigureAwait (false);
                if (isExists)
                    throw new InvalidOperationException ("Application with same permanent account number already exists");
            }

            if (!string.IsNullOrWhiteSpace (submitApplicationRequest.PersonalEmail))
            {
                isExists = await CheckIfDuplicateApplicationExists (
                    UniqueParameterTypes.Email,
                    submitApplicationRequest.PersonalEmail,
                    applicationNumber).ConfigureAwait (false);
                if (isExists) throw new InvalidOperationException ("Application with same email id already exists");
            }

            var emailVerifyResponse = "true";
            // if (ApplicationProcessorConfiguration.EnablePersonalEmailVerification)
            // {
            //     string isEmailVerify = "false";
            //     var extendedAttributes = await DataAttributesEngine.GetAttribute("application", applicationNumber, "extension");
            //     if (extendedAttributes != null)
            //     {
            //         isEmailVerify = GetEmailVerificationResult(extendedAttributes);
            //     }
            // if (isEmailVerify != "true")
            // {
            //     if (!string.IsNullOrWhiteSpace(ApplicationProcessorConfiguration.EmailValidationProvider) && ApplicationProcessorConfiguration.EmailValidationProvider.ToLower() == "emailhunter")
            //     {
            //         emailVerifyResponse = VerifyEmailAddressWithHunter(applicationNumber, submitApplicationRequest.PersonalEmail);
            //     }
            //     else if (!string.IsNullOrWhiteSpace(ApplicationProcessorConfiguration.EmailValidationProvider) && ApplicationProcessorConfiguration.EmailValidationProvider.ToLower() == "towerdata")
            //     {
            //         //emailVerifyResponse = VerifyEmailAddressWithTowerData(applicationNumber, submitApplicationRequest.PersonalEmail);
            //     }
            //     //emailVerifyResponse = VerifyEmailAddressWithHunter(submitApplicationRequest.PersonalEmail);
            // }
            // else
            // {
            //     emailVerifyResponse = isEmailVerify.ToString();
            // }
            // }

            var applicationRequest = GetApplicationRequest (submitApplicationRequest);
            // Call Email Hunter to verify the email Address

            // Application Submitted 
            IApplication application = null;
            Logger.Info ($"[OfferEngine/ApplicationService/UpdateApplication] UpdateApplication method of Application Service is called for [{applicationNumber}] application : [{  (applicationRequest != null ? JsonConvert.SerializeObject(applicationRequest) : null)}] ");
            application = await ExternalApplicationService.UpdateApplication (applicationNumber, applicationRequest);
            Logger.Info ($"[OfferEngine/ApplicationService/UpdateApplication] UpdateApplication method of Application Service is called finshed  for [{applicationNumber}] result : [{  (application != null ? JsonConvert.SerializeObject(application) : null)}] ");
            //await VerificationEngineService.Verify("application", application.ApplicationNumber, "MobileVerification",
            //    "MobileSMS", "MobileSMS", new { result = submitApplicationRequest.IsMobileVerified.ToString().ToLower() });
            string applicationStatus = "LeadCreated";
            // change status to application submitted 
            if (isValid == true && submitApplicationRequest.IsMobileVerified.ToString ().ToLower () == "true")
            {
                await EntityStatusService.ChangeStatus ("application", application.ApplicationNumber, ApplicationProcessorConfiguration.Statuses["ApplicationSubmitted"], null);
                applicationStatus = "ApplicationSubmitted";
            }

            await DataAttributesEngine.SetAttribute ("application", application.ApplicationNumber, "extension", GetApplicationExtension (submitApplicationRequest, emailVerifyResponse, ApplicationProcessorConfiguration.EnablePersonalEmailVerification, ApplicationProcessorConfiguration.EmailValidationProvider.ToLower ()));
            Logger.Info ($"[OfferEngine/ApplicationService] UpdateApplication method execution finshed for [{applicationNumber}] application : [{  (application != null ? JsonConvert.SerializeObject(application) : null)}] applicationStatus:[{applicationStatus}] ");
            return new Tuple<IApplication, string> (application, applicationStatus);
        }

        public async Task<IApplicationResponse> AddLead (ISubmitApplicationRequest submitApplicationRequest)
        {
            if (submitApplicationRequest == null)
                throw new ArgumentNullException (nameof (submitApplicationRequest));

            var applicationRequest = GetApplicationRequest (submitApplicationRequest);
            var application = await ExternalApplicationService.Add (applicationRequest);
            await DataAttributesEngine.SetAttribute ("application", application.ApplicationNumber, "extension", GetApplicationExtension (submitApplicationRequest, string.Empty, ApplicationProcessorConfiguration.EnablePersonalEmailVerification, ApplicationProcessorConfiguration.EmailValidationProvider.ToLower ()));
            await EntityStatusService.ChangeStatus ("application", application.ApplicationNumber, ApplicationProcessorConfiguration.Statuses["LeadCreated"], null);
            return application;
        }

        public async Task UpdateEmploymentDetail (string applicationNumber, IEmploymentDetail employmentInformationRequest)
        {
            EnsureInputIsValid (applicationNumber, employmentInformationRequest);
            var status = EntityStatusService.GetStatusByEntity ("application", applicationNumber).Result;

            var application = await ExternalApplicationService.GetByApplicationNumber (applicationNumber);
            if (application == null)
                throw new NotFoundException ($"Application with number {applicationNumber} is not found");

            if (status != null && status.Code == ApplicationProcessorConfiguration.Statuses["Approved"])
            {
                if (string.IsNullOrWhiteSpace (employmentInformationRequest.EmploymentId))
                {
                    await ApplicantService.AddEmployment (application.ApplicantId, employmentInformationRequest);
                }
                else
                {
                    await ApplicantService.UpdateEmployment (application.ApplicantId, employmentInformationRequest.EmploymentId, employmentInformationRequest);
                }
            }
            else
            {
                //if (status != null && ApplicationProcessorConfiguration != null && !(ApplicationProcessorConfiguration.EmploymentChangeStatus.Contains(status.Code)))
                //{
                //    throw new InvalidOperationException($"Can not update employment detail when status is {status.Code}");
                //}
                await ExternalApplicationService.UpdateEmploymentDetail (applicationNumber, employmentInformationRequest);
            }
        }

        public async Task UpdateSelfDeclaredExpense (string applicationNumber, IExpenseDetailRequest selfDeclareExpense)
        {
            if (string.IsNullOrWhiteSpace (applicationNumber))
                throw new ArgumentNullException (nameof (applicationNumber));

            if (selfDeclareExpense == null)
                throw new ArgumentNullException (nameof (selfDeclareExpense));

            if (!string.IsNullOrWhiteSpace (selfDeclareExpense.ResidenceType))
            {
                var residenceTypeExists = LookupService.GetLookupEntry ("application-residenceType", selfDeclareExpense.ResidenceType);
                if (residenceTypeExists == null)
                    throw new InvalidArgumentException ("Residence type is not valid");
            }
            var status = EntityStatusService.GetStatusByEntity ("application", applicationNumber).Result;

            if (status != null && ApplicationProcessorConfiguration != null && !(ApplicationProcessorConfiguration.ExpenseChangeStatus.Contains (status.Code)))
            {
                throw new InvalidOperationException ($"Can not update expense detail when status is {status.Code}");
            }
            await ExternalApplicationService.UpdateSelfDeclaredExpense (applicationNumber, selfDeclareExpense);
        }

        public async Task UpdateEmailInformation (string applicationNumber, IEmailAddress emailAddress)
        {
            EnsureInputIsValid (applicationNumber, emailAddress);
            var status = EntityStatusService.GetStatusByEntity ("application", applicationNumber).Result;
            var application = await ExternalApplicationService.GetByApplicationNumber (applicationNumber);

            if (application == null)
                throw new NotFoundException ($"Application with number {applicationNumber} is not found");

            if (status != null && status.Code == ApplicationProcessorConfiguration.Statuses["Approved"])
            {
                if (string.IsNullOrWhiteSpace (emailAddress.Id))
                {
                    await ApplicantService.AddEmailAddress (application.ApplicantId, emailAddress);
                }
                else
                {
                    await ApplicantService.UpdateEmailAddress (application.ApplicantId, emailAddress.Id, emailAddress);
                }
            }
            else
            {
                //ToDo - Need to define status to allow update email address
                //if (status != null && ApplicationProcessorConfiguration != null && !(ApplicationProcessorConfiguration.BankChangeStatus.Contains(status.Code)))
                //{
                //    throw new InvalidOperationException($"Can not update bank detail when status is {status.Code}");
                //}
            }
            await ExternalApplicationService.UpdateEmailInformation (applicationNumber, emailAddress);
        }

        public async Task<IEmailAddress> ReInitiateEmailVerification (string applicationNumber, IEmailAddress emailAddress)
        {
            try
            {
                var verificationDetail = await VerificationEngineService.GetStatus ("application", applicationNumber, "EmailVerifyForPersonal");
                if (verificationDetail != null && verificationDetail.CurrentStatus == LendFoundry.VerificationEngine.CurrentStatus.Completed && verificationDetail.VerificationStatus == VerificationStatus.Passed)
                {
                    throw new InvalidOperationException ($"Email address is already verified");
                }

                await UpdateEmailInformation (applicationNumber, emailAddress);

                var applicantInformation = await GetApplicationInformation (applicationNumber);

                TextInfo textInfo = new CultureInfo ("en-US", false).TextInfo;

                EmailVerificationRequest request = new EmailVerificationRequest
                {
                    Email = emailAddress.Email,
                    Source = "back-office",
                    Name = textInfo.ToTitleCase (applicantInformation.FirstName ?? string.Empty),
                    Data = null
                };

                await EmailVerificationService.InitiateEmailVerification ("application", applicationNumber, request);
                return emailAddress;
            }
            catch (Exception ex)
            {
                Logger.Info (ex.Message);
                throw;
            }
        }
        private async Task<IApplicantInformation> GetApplicationInformation (string applicationNumber)
        {
            var attributes = await DataAttributesEngine.GetAttribute ("application", applicationNumber,
                new List<string> { "application" });

            if (attributes == null || !attributes.Any ())
                throw new OfferGenerationFailedException ("No data attributes found for application source.");

            object applicationAttribute;
            if (!attributes.TryGetValue ("application", out applicationAttribute))
                throw new OfferGenerationFailedException ("application data attributes not found");

            var applicantInformation = JObject.FromObject (applicationAttribute).ToObject<ApplicantInformation> ();
            return applicantInformation;
        }

        public async Task UpdateBankInformation (string applicationNumber, IBankInformation bankInformationRequest)
        {
            EnsureInputIsValid (applicationNumber, bankInformationRequest);
            bankInformationRequest.AccountType = AccountType.Savings;
            var status = EntityStatusService.GetStatusByEntity ("application", applicationNumber).Result;
            var application = await ExternalApplicationService.GetByApplicationNumber (applicationNumber);

            if (application == null)
                throw new NotFoundException ($"Application with number {applicationNumber} is not found");

            if (application != null && status != null && status.Code == ApplicationProcessorConfiguration.Statuses["Approved"])
            {
                if (string.IsNullOrWhiteSpace (bankInformationRequest.BankId))
                {
                    await ApplicantService.AddBank (application.ApplicantId, bankInformationRequest);
                }
                else
                {
                    await ApplicantService.UpdateBank (application.ApplicantId, bankInformationRequest.BankId, bankInformationRequest);
                }
            }
            else
            {
                if (status != null && ApplicationProcessorConfiguration != null && !(ApplicationProcessorConfiguration.BankChangeStatus.Contains (status.Code)))
                {
                    throw new InvalidOperationException ($"Can not update bank detail when status is {status.Code}");
                }
                await ExternalApplicationService.UpdateBankInformation (applicationNumber, bankInformationRequest);
            }
        }

        public async Task UpdateEducationInformation (string applicationNumber, IEducationInformation educationInformationRequest)
        {
            EnsureInputIsValid (applicationNumber, educationInformationRequest);

            var application = await ExternalApplicationService.GetByApplicationNumber (applicationNumber);

            if (application == null)
                throw new NotFoundException ($"Application with number {applicationNumber} is not found");

            await ApplicantService.UpdateEducation (application.ApplicantId, educationInformationRequest);

            var applicant = await ApplicantService.Get (application.ApplicantId);

            if (applicant == null)
                throw new NotFoundException ($"Applicant with number {application.ApplicantId} is not found");

            await EventHub.Publish (new ApplicationModified { Application = application, Applicant = applicant });

        }

        public async Task UpdatePersonalDetails (string applicationNumber, UpdateApplicantRequest personalDetails)
        {
            EnsureInputIsValid (applicationNumber, personalDetails);
            var status = EntityStatusService.GetStatusByEntity ("application", applicationNumber).Result;

            if (status != null && ApplicationProcessorConfiguration != null && !(ApplicationProcessorConfiguration.UpdatePersonalDetails.Contains (status.Code)))
            {
                throw new InvalidOperationException ($"Can not update personal detail when status is {status.Code}");
            }
            var application = await ExternalApplicationService.GetByApplicationNumber (applicationNumber);

            if (application == null)
                throw new NotFoundException ($"Application with number {applicationNumber} is not found");

            var applicant = await ApplicantService.UpdateApplicant (application.ApplicantId, personalDetails);
            await EventHub.Publish (new ApplicationModified { Application = application, Applicant = applicant });
        }

        public async Task UpdateAddress (string applicationNumber, IAddress address)
        {
            EnsureInputIsValid (applicationNumber, address);
            var status = EntityStatusService.GetStatusByEntity ("application", applicationNumber).Result;
            var application = await ExternalApplicationService.GetByApplicationNumber (applicationNumber);

            if (application == null)
                throw new NotFoundException ($"Application with number {applicationNumber} is not found");

            if (status != null && status.Code == ApplicationProcessorConfiguration.Statuses["Approved"])
            {
                if (string.IsNullOrWhiteSpace (address.AddressId))
                {
                    await ApplicantService.AddAddress (application.ApplicantId, address);
                }
                else
                {
                    await ApplicantService.UpdateAddress (application.ApplicantId, address.AddressId, address);
                }
            }
            else
            {
                //ToDo - Need to define status to allow update email address
                if (status != null && ApplicationProcessorConfiguration != null && !(ApplicationProcessorConfiguration.AddressChangeStatus.Contains (status.Code)))
                {
                    throw new InvalidOperationException ($"Can not update address detail when status is {status.Code}");
                }
            }
            await ExternalApplicationService.UpdateAddress (applicationNumber, address);
        }

        private static ApplicationExtension GetApplicationExtension (ISubmitApplicationRequest submitApplicationRequest, string emailVerifyResponse, bool isVerificationEnable, string provider)
        {
            //TODO: add property for Religion (string),MothersMaidenName(string),Category(string)
            var extension = new ApplicationExtension ();

            extension.MobileVerificationDetail = new MobileVerificationRequest
            {
                IsVerified = submitApplicationRequest.IsMobileVerified,
                VerificationTime = submitApplicationRequest.MobileVerificationTime ?? null, //.HasValue?submitApplicationRequest.MobileVerificationTime.Value:,
                Notes = submitApplicationRequest.MobileVerificationNotes
            };
            if (!isVerificationEnable)
            {
                emailVerifyResponse = "Not Verified";
                provider = string.Empty;
            }

            extension.EmailVerificationResult = emailVerifyResponse;
            extension.EmailVerificationProvider = provider;

            return extension;
        }

        public async Task<IAncillaryData> UpdateAncillaryData (string applicationNumber, IAncillaryData ancillaryData)
        {
            if (!Enum.IsDefined (typeof (Category), ancillaryData.Category))
                throw new InvalidEnumArgumentException ("The value of argument " + nameof (ancillaryData.Category) + " is invalid for Enum type Category");

            if (!Enum.IsDefined (typeof (Religion), ancillaryData.Religion))
                throw new InvalidEnumArgumentException ("The value of argument " + nameof (ancillaryData.Religion) + " is invalid for Enum type Religion");

            //TODO: It should use the same interface as Religion (string),MothersMaidenName(string),Category(string)
            //TODO: It will get existing extension from the Data-Attribute, set the new value and update it.
            if (string.IsNullOrWhiteSpace (applicationNumber))
                throw new ArgumentException ($"{nameof(applicationNumber)} is mandatory");

            await DataAttributesEngine.SetAttribute ("application", applicationNumber, "ancillaryData", ancillaryData);
            return ancillaryData;
        }

        public async Task<ILoanFundedData> UpdateLoanFundedData (string applicationNumber, ILoanFundedData loanFundedData)
        {
            if (string.IsNullOrWhiteSpace (applicationNumber))
                throw new ArgumentException ($"{nameof(applicationNumber)} is mandatory");

            await DataAttributesEngine.SetAttribute ("application", applicationNumber, "loan-fundeddata", loanFundedData);
            var currentstatus = EntityStatusService.GetStatusByEntity ("application", applicationNumber).Result;
            if (currentstatus.Code != "200.12" && !string.IsNullOrEmpty (loanFundedData.LoanAccountNumber) && !string.IsNullOrEmpty (loanFundedData.UTRNumber))
            {
                await Task.Run (() => EntityStatusService.ChangeStatus ("application", applicationNumber, ApplicationProcessorConfiguration.Statuses["Funded"], null));
            }
            await EventHub.Publish (new FundingDataUpdated ()
            {
                EntityId = applicationNumber,
                    EntityType = "application",
                    Response = loanFundedData,
                    Request = loanFundedData,
                    ReferenceNumber = Guid.NewGuid ().ToString ("N")
            });
            return loanFundedData;
        }

        // private string VerifyEmailAddressWithHunter(string applicationNumber, string emailAddress)
        // {
        //     var result = string.Empty;
        //     try
        //     {
        //         var syndicationResult = CallEmailHunterEmailVerification(emailAddress);

        //         if (syndicationResult != null)
        //         {
        //             object executionResult = DecisionEngineService.Execute<dynamic, dynamic>(
        //                 "emailhunter_verify_emailrules",
        //                 new { payload = syndicationResult });

        //             if (executionResult != null)
        //             {
        //                 result = GetResultValue(executionResult);
        //             }
        //             if (result != null)
        //             {
        //                 if (result.ToLower() == "true")
        //                 {
        //                     DismissAlertForApplication(applicationNumber, "Personal Email Verification");
        //                     return result;
        //                 }
        //             }
        //         }
        //       Task.Run(()=> GenerateAlertForEmailVerification(applicationNumber, emailAddress, syndicationResult));
        //     }
        //     catch (Exception ex)
        //     {
        //         Logger.Error(ex.Message);
        //         Task.Run(() => GenerateAlertForEmailVerification(applicationNumber, emailAddress, null));
        //         //EventHub.Publish(new PersonalEmailVerificationFailed { ApplicationNumber = applicationNumber, EmailAddress = emailAddress, EmailValidationProvider = ApplicationProcessorConfiguration.EmailValidationProvider });
        //         result = "false";
        //     }
        //     return result;
        // }
        //private string VerifyEmailAddressWithTowerData(string applicationNumber, string emailAddress)
        //{
        //    var result = string.Empty;
        //    try
        //    {
        //        var syndicationResult = CallTowerDataEmailVerification(emailAddress);
        //        //TODO: Change exception to meaning full
        //        if (syndicationResult != null)
        //        {
        //            var resultCode = syndicationResult.StatusCode;
        //            if (resultCode == 5)
        //            {
        //                syndicationResult = CallTowerDataEmailVerification(emailAddress);
        //            }

        //            object executionResult = DecisionEngineService.Execute<dynamic, dynamic>(
        //                "verification_rule_towerdataemail",
        //                new { payload = syndicationResult });

        //            if (executionResult != null)
        //            {
        //                result = executionResult.ToString();
        //            }

        //            if (result.ToLower() == "true")
        //            {
        //                DismissAlertForApplication(applicationNumber, "Personal Email Verification");
        //                return result;
        //            }

        //        }
        //        Task.Run(() => GenerateAlertForEmailVerification(applicationNumber, emailAddress, syndicationResult));
        //        //EventHub.Publish(new PersonalEmailVerificationFailed { ApplicationNumber = applicationNumber, EmailAddress = emailAddress, SyndicationResult = syndicationResult, EmailValidationProvider = ApplicationProcessorConfiguration.EmailValidationProvider });
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.Error(ex.Message);
        //        Task.Run(() => GenerateAlertForEmailVerification(applicationNumber, emailAddress, null));
        //        //EventHub.Publish(new PersonalEmailVerificationFailed { ApplicationNumber = applicationNumber, EmailAddress = emailAddress, EmailValidationProvider = ApplicationProcessorConfiguration.EmailValidationProvider });
        //        result = "false";
        //    }
        //    return result;
        //}

        private async Task GenerateAlertForEmailVerification (string applicationNumber, string emailAddress, object syndicationResult)
        {
            var alerts = AlertEngineService.GetAlerts ("application", applicationNumber, AlertStatus.Undefined);
            if (alerts != null && alerts.Any ())
            {
                var alert = alerts.Where (a => a.Name == "Personal Email Verification").FirstOrDefault ();
                if (alert == null)
                {
                    await EventHub.Publish (new PersonalEmailVerificationFailed { ApplicationNumber = applicationNumber, EmailAddress = emailAddress, SyndicationResult = syndicationResult, EmailValidationProvider = ApplicationProcessorConfiguration.EmailValidationProvider });
                }
                else
                {
                    if (alert.Status != AlertStatus.Active)
                    {
                        AlertEngineService.Undo ("application", applicationNumber, alert.Id);
                    }
                }
            }
            else
            {
                await EventHub.Publish (new PersonalEmailVerificationFailed { ApplicationNumber = applicationNumber, EmailAddress = emailAddress, SyndicationResult = syndicationResult, EmailValidationProvider = ApplicationProcessorConfiguration.EmailValidationProvider });
            }
        }

        private void DismissAlertForApplication (string applicationNumber, string alertName)
        {
            var alerts = AlertEngineService.GetAlerts ("application", applicationNumber, AlertStatus.Undefined);
            if (alerts != null && alerts.Any ())
            {
                var alert = alerts.OrderByDescending (x => x.Timestamp).Where (a => a.Name == alertName && a.Status == AlertStatus.Active).FirstOrDefault ();
                if (alert != null)
                {
                    AlertEngineService.Dismiss ("application", applicationNumber, alert.Id);
                }
            }
        }
        // private IEmailVerificationResult CallEmailHunterEmailVerification(string email)
        // {
        //     IEmailVerificationResult emailVerificationResponse = null;

        //     FaultRetry.RunWithAlwaysRetry(() =>
        //     {
        //         emailVerificationResponse = EmailHunterService.EmailVerifier("application", email).Result;
        //     });

        //     return emailVerificationResponse;
        // }

        //private IEmailVerificationResponse CallTowerDataEmailVerification(string email)
        //{
        //    IEmailVerificationResponse emailVerificationResponse = null;

        //    FaultRetry.RunWithAlwaysRetry(() =>
        //    {
        //        emailVerificationResponse = TowerDataService.VerifyEmail("application", email).Result;
        //    });

        //    return emailVerificationResponse;
        //}
        private IPrimaryApplicantDetail GetApplicantRequest (ISubmitApplicationRequest request)
        {
            //DateTimeOffset? test = null;
            //request.DateOfBirth = test;
            IPrimaryApplicantDetail applicantRequest = new PrimaryApplicantDetail ();
            applicantRequest.AadhaarNumber = request.AadhaarNumber;
            applicantRequest.DateOfBirth = request.DateOfBirth; //request.DateOfBirth.HasValue ? request.DateOfBirth.Value : null;
            applicantRequest.FirstName = request.FirstName;
            applicantRequest.MiddleName = request.MiddleName;
            applicantRequest.LastName = request.LastName;
            applicantRequest.Password = request.Password;
            applicantRequest.UserName = request.UserName;
            applicantRequest.UserId = request.UserId;
            applicantRequest.Salutation = request.Salutation;
            applicantRequest.PermanentAccountNumber = request.PermanentAccountNumber;
            applicantRequest.MaritalStatus = request.MaritalStatus;
            //if (!string.IsNullOrWhiteSpace(request.CurrentAddressLine1))
            //{
            applicantRequest.Addresses = new List<IAddress>
            {
                new Address
                {
                AddressLine1 = request.CurrentAddressLine1,
                AddressLine2 = request.CurrentAddressLine2,
                AddressLine3 = request.CurrentAddressLine3,
                AddressLine4 = request.CurrentAddressLine4,
                AddressType = AddressType.Current,
                City = request.CurrentCity,
                Country = request.CurrentCity,
                PinCode = request.CurrentPinCode,
                LandMark = request.CurrentLandMark,
                Location = request.CurrentLocation,
                State = request.CurrentState,
                IsDefault = true
                },
                new Address
                {
                AddressLine1 = request.PermanentAddressLine1,
                AddressLine2 = request.PermanentAddressLine2,
                AddressLine3 = request.PermanentAddressLine3,
                AddressLine4 = request.PermanentAddressLine4,
                AddressType = AddressType.Permanant,
                City = request.PermanentCity,
                Country = request.PermanentCountry,
                PinCode = request.PermanentPinCode,
                LandMark = request.PermanenttLandMark,
                Location = request.PermanentLocation,
                State = request.PermanentState
                }
            };
            // }
            //if (!string.IsNullOrWhiteSpace(request.PermanentAddressLine1))
            //{
            //if (applicantRequest.Addresses != null)
            //{
            //    applicantRequest.Addresses.Add(new Address
            //    {
            //        AddressLine1 = request.PermanentAddressLine1,
            //        AddressLine2 = request.PermanentAddressLine2,
            //        AddressLine3 = request.PermanentAddressLine3,
            //        AddressLine4 = request.PermanentAddressLine4,
            //        AddressType = AddressType.Permanant,
            //        City = request.PermanentCity,
            //        Country = request.PermanentCountry,
            //        PinCode = request.PermanentPinCode,
            //        LandMark = request.PermanenttLandMark,
            //        Location = request.PermanentLocation,
            //        State = request.PermanentState
            //    });
            //};

            // }

            //if (!string.IsNullOrWhiteSpace(request.EmployerName))
            //{
            applicantRequest.EmploymentDetail = new EmploymentDetail
            {
                //Addresses = string.IsNullOrEmpty(request.WorkAddressLine1) ? null : new List<IAddress>
                Addresses = new List<IAddress>
                {
                new Address
                {
                AddressId = Guid.NewGuid ().ToString ("N"),
                AddressLine1 = request.WorkAddressLine1,
                AddressLine2 = request.WorkAddressLine2,
                AddressLine3 = request.WorkAddressLine3,
                AddressLine4 = request.WorkAddressLine4,
                AddressType = AddressType.Work,
                City = request.WorkCity,
                Country = request.WorkCountry,
                PinCode = request.WorkPinCode,
                LandMark = request.WorkLandMark,
                Location = request.WorkLocation,
                State = request.WorkState,
                IsDefault = true
                }
                },
                AsOfDate = request.EmploymentAsOfDate,
                CinNumber = request.CinNumber,
                Designation = request.Designation,
                EmploymentStatus = request.EmploymentStatus,
                LengthOfEmploymentInMonths = request.LengthOfEmploymentInMonths,
                Name = request.EmployerName,
                WorkEmail = request.WorkEmail,
                WorkPhoneNumbers = string.IsNullOrEmpty (request.WorkMobile) ? null : new List<IPhoneNumber>
                {
                new PhoneNumber
                {

                CountryCode = "91",
                IsDefault = true,
                Phone = request.WorkMobile,
                PhoneType = PhoneType.Work
                }
                },
                // IncomeInformation = request.Income <= 0 ? null : new IncomeInformation
                IncomeInformation = new IncomeInformation
                {
                Income = request.Income,
                // PaymentFrequency = request.PaymentFrequency == 0 ? (PaymentFrequency)Enum.ToObject(typeof(PaymentFrequency), ApplicationProcessorConfiguration.DefaultPaymentFrequency) : request.PaymentFrequency,
                PaymentFrequency = request.PaymentFrequency
                }
            };
            //};
            applicantRequest.Gender = request.Gender;
            applicantRequest.EmailAddress = new EmailAddress
            {
                Email = request.PersonalEmail,
                EmailType = Applicant.EmailType.Personal,
                IsDefault = true
            };
            applicantRequest.PhoneNumbers = new List<IPhoneNumber>
            {
                new PhoneNumber
                {
                CountryCode = "91",
                IsDefault = true,
                Phone = request.PersonalMobile,
                PhoneType = PhoneType.Mobile
                }
            };
            applicantRequest.EducationInformation = new EducationInformation
            {
                EducationalInstitution = request.EducationalInstitution,
                LevelOfEducation = request.LevelOfEducation

            };
            return applicantRequest;
        }

        private IApplicationRequest GetApplicationRequest (ISubmitApplicationRequest request)
        {
            IApplicationRequest applicationRequest = new ApplicationRequest
            {

                PurposeOfLoan = request.PurposeOfLoan,
                RequestedAmount = request.RequestedAmount,
                //RequestedTermType = request.RequestedTermType == 0 ? (LoanFrequency)Enum.ToObject(typeof(LoanFrequency), ApplicationProcessorConfiguration.DefaultLoanTerm) : request.RequestedTermType,
                RequestedTermType = request.RequestedTermType,
                RequestedTermValue = request.RequestedTermValue,
                ResidenceType = request.ResidenceType,
                OtherPurposeDescription = request.OtherPurposeDescription,
                SodexoCode = request.SodexoCode,
                CurrentAddressMonth = request.CurrentAddressMonth,
                CurrentAddressYear = request.CurrentAddressYear,
                EmployementMonth = request.EmployementMonth,
                EmployementYear = request.EmployementYear,
                PrimaryApplicant = GetApplicantRequest (request)
            };

            //if (!string.IsNullOrWhiteSpace(request.EmployerName))
            //{
            applicationRequest.EmploymentDetail = new EmploymentDetail
            {
                //Addresses = string.IsNullOrEmpty(request.WorkAddressLine1) ? null : new List<IAddress>
                Addresses = new List<IAddress>
                {
                new Address
                {
                AddressId = Guid.NewGuid ().ToString ("N"),
                AddressLine1 = request.WorkAddressLine1,
                AddressLine2 = request.WorkAddressLine2,
                AddressLine3 = request.WorkAddressLine3,
                AddressLine4 = request.WorkAddressLine4,
                AddressType = AddressType.Work,
                City = request.WorkCity,
                Country = request.WorkCountry,
                PinCode = request.WorkPinCode,
                LandMark = request.WorkLandMark,
                Location = request.WorkLocation,
                State = request.WorkState,
                IsDefault = true
                }
                },
                AsOfDate = request.EmploymentAsOfDate,
                CinNumber = request.CinNumber,
                Designation = request.Designation,
                EmploymentStatus = request.EmploymentStatus,
                LengthOfEmploymentInMonths = request.LengthOfEmploymentInMonths,
                Name = request.EmployerName,
                WorkEmail = request.WorkEmail,
                WorkPhoneNumbers = string.IsNullOrEmpty (request.WorkMobile) ? null : new List<IPhoneNumber>
                {
                new PhoneNumber
                {

                CountryCode = "91",
                IsDefault = true,
                Phone = request.WorkMobile,
                PhoneType = PhoneType.Work
                }
                },
                IncomeInformation = request.Income <= 0 ? null : new IncomeInformation
                {
                Income = request.Income,
                // PaymentFrequency = request.PaymentFrequency == 0 ? (PaymentFrequency)Enum.ToObject(typeof(PaymentFrequency), ApplicationProcessorConfiguration.DefaultPaymentFrequency) : request.PaymentFrequency,
                PaymentFrequency = request.PaymentFrequency,

                }
            };
            // };
            //request.SystemChannel = request.SystemChannel == 0 ? SystemChannel.BorrowerPortal : request.SystemChannel;
            //request.SourceType = request.SourceType == 0 ? SourceType.Organic : request.SourceType;
            //request.SourceReferenceId = string.IsNullOrEmpty(request.SourceReferenceId)? "BorrowerPortal"  : request.SourceReferenceId;
            //request.TrackingCode = string.IsNullOrEmpty(request.TrackingCode) ? "BorrowerPortal" : request.TrackingCode;
            applicationRequest.SelfDeclareExpense = new SelfDeclareExpense
            {
                CreditCardBalances = request.CreditCardBalances,
                DebtPayments = request.DebtPayments,
                MonthlyExpenses = request.MonthlyExpenses,
                MonthlyRent = request.MonthlyRent
            };

            //if (request.SourceType != null)
            //{
            applicationRequest.Source = new Application.Source
            {
                SourceReferenceId = request.SourceReferenceId,
                SourceType = request.SourceType,
                SystemChannel = request.SystemChannel,
                TrackingCode = request.TrackingCode,
                TrackingCodeMedium = request.TrackingCodeMedium,
                GCLId = request.GCLId,
                TrackingCodeCampaign = request.TrackingCodeCampaign,
                TrackingCodeTerm = request.TrackingCodeTerm,
                TrackingCodeContent = request.TrackingCodeContent

            };
            //  };
            return applicationRequest;
        }

        // private static string GetEmailVerificationResult(dynamic data)
        // {
        //     Func<dynamic> resultProperty = () => data.EmailHunterVerificationResult;

        //     return HasProperty(resultProperty) ? GetValue(resultProperty) : string.Empty;
        // }

        private static string GetBankValue (dynamic data)
        {
            Func<dynamic> resultProperty = () => data.BankPreferenceSource;

            return HasProperty (resultProperty) ? GetValue (resultProperty) : string.Empty;
        }

        private static string GetResultValue (dynamic data)
        {
            Func<dynamic> resultProperty = () => data.result;

            return HasProperty (resultProperty) ? GetValue (resultProperty) : string.Empty;
        }
        private static bool HasProperty<T> (Func<T> property)
        {
            try
            {
                property ();
                return true;
            }
            catch (RuntimeBinderException)
            {
                return false;
            }
        }

        private static T GetValue<T> (Func<T> property)
        {
            return property ();
        }

        private bool EnsureInputIsValid (ISubmitApplicationRequest applicationRequest, out string reason)
        {
            var isValid = true;
            reason = string.Empty;
            if (applicationRequest == null)
                throw new ArgumentNullException (nameof (applicationRequest), "application cannot be empty");

            if (string.IsNullOrEmpty (applicationRequest.PersonalMobile))
            {
                reason = "Personal mobile number is blank";
                return false;
            }

            if (string.IsNullOrEmpty (applicationRequest.PersonalEmail))
            {
                reason = "Personal email address is blank";
                return false;
            }

            //if (applicationRequest.MobileVerificationTime == null)
            //    throw new ArgumentException("Mobile verification time mandatory");

            if (applicationRequest.IsMobileVerified == false)
            {
                reason = "Personal mobile is not verified";
                return false;
            }

            if (applicationRequest.RequestedAmount <= 0)
            {
                reason = $"Requested amount {applicationRequest.RequestedAmount} is below minimum expected amount";
                return false;
            }

            //if (applicationRequest.RequestedTermType != null && !Enum.IsDefined(typeof(LoanFrequency), applicationRequest.RequestedTermType))
            //{
            //    reason = $"Requested term type {applicationRequest.RequestedTermType} is invalid";
            //    return isValid = false;
            //}

            if (!Enum.IsDefined (typeof (PaymentFrequency), applicationRequest.PaymentFrequency))
            {
                reason = $"Payment frequency {applicationRequest.PaymentFrequency} is invalid";
                return false;
            }

            if (!Enum.IsDefined (typeof (SystemChannel), applicationRequest.SystemChannel))
            {
                reason = $"System channel {applicationRequest.SystemChannel} is invalid";
                return false;
            }

            if (string.IsNullOrWhiteSpace (applicationRequest.PurposeOfLoan))
            {
                reason = $"Purpose of loan {applicationRequest.PurposeOfLoan} is invalid";
                return isValid = false;
            }

            var purposeOfLoanExists = LookupService.GetLookupEntry ("application-purposeOfLoan", applicationRequest.PurposeOfLoan);
            if (purposeOfLoanExists == null)
            {
                reason = $"Purpose of loan {applicationRequest.PurposeOfLoan} is invalid";
                return isValid = false;
            }

            if (string.IsNullOrWhiteSpace (applicationRequest.ResidenceType))
                return isValid = false;
            var residenceTypeExists = LookupService.GetLookupEntry ("application-residenceType", applicationRequest.ResidenceType);
            if (residenceTypeExists == null)
            {
                reason = $"Residence type {applicationRequest.ResidenceType} is invalid";
                return isValid = false;
            }

            if (!Enum.IsDefined (typeof (SourceType), applicationRequest.SourceType))
            {
                reason = $"Source type {applicationRequest.SourceType} is invalid";
                return isValid = false;
            }

            if (!(applicationRequest.SourceType == SourceType.Organic) && string.IsNullOrEmpty (applicationRequest.SourceReferenceId))
            {
                reason = $"SourceReferenceId can not be blank for {applicationRequest.SourceType}";
                return isValid = false;
            }

            if (string.IsNullOrEmpty (applicationRequest.UserName) && string.IsNullOrEmpty (applicationRequest.Password))
            {
                if (string.IsNullOrEmpty (applicationRequest.UserId))
                {
                    reason = $"UserId is blank";
                    return isValid = false;
                }
            }

            if (string.IsNullOrEmpty (applicationRequest.UserId))
            {
                if (string.IsNullOrEmpty (applicationRequest.UserName))
                {
                    reason = $"User name is blank";
                    return false;
                }

                if (string.IsNullOrEmpty (applicationRequest.Password))
                {
                    reason = $"Password is blank";
                    return false;
                }
            }
            if (string.IsNullOrEmpty (applicationRequest.Salutation))
            {
                reason = "Salutation is blank";
                return false;
            }

            if (string.IsNullOrEmpty (applicationRequest.FirstName))
            {
                reason = "FirstName is blank";
                return false;
            }

            if (string.IsNullOrEmpty (applicationRequest.LastName))
            {
                reason = "LastName is blank";
                return false;
            }

            if (applicationRequest.DateOfBirth == null)
            {
                reason = $"DateOfBirth is blank";
                return isValid = false;
            }

            if (!Enum.IsDefined (typeof (MaritalStatus), applicationRequest.MaritalStatus))
            {
                reason = $"Marital status {applicationRequest.MaritalStatus} is invalid";
                return isValid = false;
            }

            if (!Enum.IsDefined (typeof (Applicant.Gender), applicationRequest.Gender))
            {
                reason = $"Gender {applicationRequest.Gender} is invalid";
                return isValid = false;
            }

            if (string.IsNullOrWhiteSpace (applicationRequest.PermanentAccountNumber))
            {
                reason = $"Permanent account number is blank";
                return isValid = false;
            }

            if (string.IsNullOrWhiteSpace (applicationRequest.AadhaarNumber))
            {
                reason = $"Aadhaar number is blank";
                return isValid = false;
            }
            if (applicationRequest.AadhaarNumber.Length != 12)
            {
                reason = $"Aadhaar number length is invalid";
                return isValid = false;
            }
            if (string.IsNullOrWhiteSpace (applicationRequest.CurrentAddressLine1))
            {
                reason = "AddressLine1 for current address is blank";
                return false;
            }

            if (string.IsNullOrWhiteSpace (applicationRequest.CurrentCity))
            {
                reason = "City for current address is blank";
                return false;
            }

            if (string.IsNullOrWhiteSpace (applicationRequest.CurrentState))
            {
                reason = "State for current address is blank";
                return false;
            }

            if (string.IsNullOrWhiteSpace (applicationRequest.CurrentPinCode))
            {
                reason = "PinCode for current address is blank";
                return false;
            }

            if (string.IsNullOrWhiteSpace (applicationRequest.CurrentCountry))
            {
                reason = "Country for current address is blank";
                return false;
            }

            if (string.IsNullOrWhiteSpace (applicationRequest.PermanentAddressLine1))
            {
                reason = "AddressLine1 for permanent address is blank";
                return false;
            }

            if (string.IsNullOrWhiteSpace (applicationRequest.PermanentCity))
            {
                reason = "City for permanent address is blank";
                return false;
            }

            if (string.IsNullOrWhiteSpace (applicationRequest.PermanentState))
            {
                reason = $"State for permanent address is blank";
                return false;
            }

            if (string.IsNullOrWhiteSpace (applicationRequest.PermanentPinCode))
            {
                reason = "PinCode for permanent address is blank";
                return false;
            }

            if (string.IsNullOrWhiteSpace (applicationRequest.PermanentCountry))
            {
                reason = "Country for permanent address is blank";
                return false;
            }

            if (string.IsNullOrWhiteSpace (applicationRequest.EmployerName))
            {
                reason = "Employer Name address is blank";
                return false;
            }

            if (string.IsNullOrEmpty (applicationRequest.WorkEmail))
            {
                reason = "Work email address is blank";
                return false;
            }

            if (applicationRequest.Income <= 0)
            {
                reason = "Income is less than or equal to zero";
                return false;
            }

            if (string.IsNullOrEmpty (applicationRequest.CinNumber))
            {
                reason = "Cin Nnmber is blank";
                return false;
            }

            if (!Enum.IsDefined (typeof (EmploymentStatus), applicationRequest.EmploymentStatus))
            {
                reason = $"Employment status {applicationRequest.EmploymentStatus} is invalid";
                return false;
            }
            // residential address validation can not be zero
            if (applicationRequest.CurrentAddressYear == "0")
            {
                reason = "Current Residential Address Year can not be zero";
                return false;
            }
            return true;
        }

        private void EnsureInputIsValid_old (ISubmitApplicationRequest applicationRequest)
        {
            if (applicationRequest == null)
                throw new ArgumentNullException (nameof (applicationRequest), "application cannot be empty");

            if (string.IsNullOrEmpty (applicationRequest.PersonalMobile))
                throw new ArgumentException ("Personal mobile Number is mandatory");

            if (string.IsNullOrEmpty (applicationRequest.PersonalEmail))
                throw new ArgumentException ("Personal email address is mandatory");

            //if (applicationRequest.MobileVerificationTime == null)
            //    throw new ArgumentException("Mobile verification time mandatory");

            if (applicationRequest.IsMobileVerified == false)
                throw new ArgumentException ("Mobile Number is not verified for this application");

            if (applicationRequest.RequestedAmount <= 0)
                throw new ArgumentNullException ("Requested amount is mandatory");

            if (!Enum.IsDefined (typeof (LoanFrequency), applicationRequest.RequestedTermType))
                throw new InvalidEnumArgumentException (nameof (applicationRequest.RequestedTermType), (int) applicationRequest.RequestedTermType, typeof (LoanFrequency));

            if (!Enum.IsDefined (typeof (PaymentFrequency), applicationRequest.PaymentFrequency))
                throw new InvalidEnumArgumentException (nameof (applicationRequest.PaymentFrequency), (int) applicationRequest.PaymentFrequency, typeof (PaymentFrequency));

            if (!Enum.IsDefined (typeof (SystemChannel), applicationRequest.SystemChannel))
                throw new InvalidEnumArgumentException (nameof (applicationRequest.SystemChannel), (int) applicationRequest.SystemChannel, typeof (SystemChannel));

            if (string.IsNullOrWhiteSpace (applicationRequest.PurposeOfLoan))
                throw new ArgumentNullException ("Purpose Of Loan  is mandatory");

            if (string.IsNullOrWhiteSpace (applicationRequest.PurposeOfLoan))
                throw new ArgumentNullException ("Purpose Of Loan  is mandatory");
            var purposeOfLoanExists = LookupService.GetLookupEntry ("application-purposeOfLoan", applicationRequest.PurposeOfLoan);
            if (purposeOfLoanExists == null)
                throw new InvalidArgumentException ("Purpose of loan is not valid");

            if (string.IsNullOrWhiteSpace (applicationRequest.ResidenceType))
                throw new ArgumentNullException ("Residence type is mandatory");
            var residenceTypeExists = LookupService.GetLookupEntry ("application-residenceType", applicationRequest.ResidenceType);
            if (residenceTypeExists == null)
                throw new InvalidArgumentException ("Residence type is not valid.");

            if (!Enum.IsDefined (typeof (SourceType), applicationRequest.SourceType))
                throw new InvalidEnumArgumentException (nameof (applicationRequest.SourceType), (int) applicationRequest.SourceType, typeof (SourceType));

            if (!(applicationRequest.SourceType == SourceType.Organic) && string.IsNullOrEmpty (applicationRequest.SourceReferenceId))
                throw new ArgumentNullException ("Source reference id is mandatory");

            if (string.IsNullOrEmpty (applicationRequest.UserName) && string.IsNullOrEmpty (applicationRequest.Password))
            {
                if (string.IsNullOrEmpty (applicationRequest.UserId))
                    throw new ArgumentNullException ($"{nameof(applicationRequest.UserId)} is mandatory");
            }

            if (string.IsNullOrEmpty (applicationRequest.UserId))
            {
                if (string.IsNullOrEmpty (applicationRequest.UserName))
                    throw new ArgumentNullException ($"{nameof(applicationRequest.UserName)} is mandatory");

                if (string.IsNullOrEmpty (applicationRequest.Password))
                    throw new ArgumentNullException ($"{nameof(applicationRequest.Password)} is mandatory");
            }
            if (string.IsNullOrEmpty (applicationRequest.Salutation))
                throw new ArgumentNullException ($"{nameof(applicationRequest.Salutation)} is mandatory");

            if (string.IsNullOrEmpty (applicationRequest.FirstName))
                throw new ArgumentNullException ($"{nameof(applicationRequest.FirstName)} is mandatory");

            if (string.IsNullOrEmpty (applicationRequest.LastName))
                throw new ArgumentNullException ($"{nameof(applicationRequest.LastName)} is mandatory");

            if (applicationRequest.DateOfBirth == null)
                throw new ArgumentNullException ($"{nameof(applicationRequest.DateOfBirth)} is mandatory");

            if (!Enum.IsDefined (typeof (MaritalStatus), applicationRequest.MaritalStatus))
                throw new InvalidEnumArgumentException (nameof (applicationRequest.MaritalStatus), (int) applicationRequest.MaritalStatus, typeof (MaritalStatus));

            if (!Enum.IsDefined (typeof (Applicant.Gender), applicationRequest.Gender))
                throw new InvalidEnumArgumentException (nameof (applicationRequest.Gender), (int) applicationRequest.Gender, typeof (Applicant.Gender));

            if (string.IsNullOrWhiteSpace (applicationRequest.PermanentAccountNumber))
                throw new ArgumentException ($"{nameof(applicationRequest.PermanentAccountNumber)} is mandatory");

            if (string.IsNullOrWhiteSpace (applicationRequest.AadhaarNumber))
                throw new ArgumentException ($"{nameof(applicationRequest.AadhaarNumber)} is mandatory");

            if (string.IsNullOrWhiteSpace (applicationRequest.CurrentAddressLine1))
                throw new ArgumentException ("Current address line1 is mandatory");

            //if (string.IsNullOrWhiteSpace(applicationRequest.CurrentAddressLine2))
            //    throw new ArgumentException("Current address line2 is mandatory");

            //if (string.IsNullOrWhiteSpace(applicationRequest.CurrentLocation))
            //    throw new ArgumentException($"Current address location is mandatory");

            if (string.IsNullOrWhiteSpace (applicationRequest.CurrentCity))
                throw new ArgumentException ("Current address city is mandatory");

            if (string.IsNullOrWhiteSpace (applicationRequest.CurrentState))
                throw new ArgumentException ("Current address state is mandatory");

            if (string.IsNullOrWhiteSpace (applicationRequest.CurrentPinCode))
                throw new ArgumentException ("Current address pincode is mandatory");

            if (string.IsNullOrWhiteSpace (applicationRequest.CurrentCountry))
                throw new ArgumentException ($"Current address country is mandatory");

            if (string.IsNullOrWhiteSpace (applicationRequest.PermanentAddressLine1))
                throw new ArgumentException ("Permanent address line1 is mandatory");

            //if (string.IsNullOrWhiteSpace(applicationRequest.PermanentAddressLine2))
            //    throw new ArgumentException("Permanent address line2 is mandatory");

            //if (string.IsNullOrWhiteSpace(applicationRequest.PermanentLocation))
            //    throw new ArgumentException($"Permanent address location is mandatory");

            if (string.IsNullOrWhiteSpace (applicationRequest.PermanentCity))
                throw new ArgumentException ("Permanent address city is mandatory");

            if (string.IsNullOrWhiteSpace (applicationRequest.PermanentState))
                throw new ArgumentException ("Permanent address state is mandatory");

            if (string.IsNullOrWhiteSpace (applicationRequest.PermanentPinCode))
                throw new ArgumentException ("Permanent address pincode is mandatory");

            if (string.IsNullOrWhiteSpace (applicationRequest.PermanentCountry))
                throw new ArgumentException ($"Permanent address country is mandatory");

            if (string.IsNullOrWhiteSpace (applicationRequest.EmployerName))
                throw new ArgumentException ($"Employer name is mandatory");

            if (string.IsNullOrEmpty (applicationRequest.WorkEmail))
                throw new ArgumentException ($"Work email id is mandatory");

            if (applicationRequest.Income <= 0)
                throw new ArgumentException ($"Income is mandatory");

            if (string.IsNullOrEmpty (applicationRequest.CinNumber))
                throw new ArgumentException ($"Cin number is mandatory");

            if (!Enum.IsDefined (typeof (EmploymentStatus), applicationRequest.EmploymentStatus))
                throw new InvalidEnumArgumentException ($"Employment status is not valid");

            //if (applicationRequest.CreditCardBalances <= 0)
            //    throw new ArgumentException($"Credit card balances is mandatory");

            //if (applicationRequest.MonthlyExpenses <= 0)
            //    throw new ArgumentException($"Monthly expenses is mandatory");

            //if (applicationRequest.MonthlyRent <= 0)
            //    throw new ArgumentException($"Monthly rent is mandatory");

        }
        private bool EnsureInputIsValidForeNewLead (ISubmitApplicationRequest applicationRequest, out string reason)
        {
            var isValid = true;
            reason = string.Empty;
            if (applicationRequest == null)
                throw new ArgumentNullException (nameof (applicationRequest), "application cannot be empty");

            if (string.IsNullOrEmpty (applicationRequest.PersonalMobile))
            {
                reason = "Personal mobile number is blank";
                return false;
            }

            if (string.IsNullOrEmpty (applicationRequest.PersonalEmail))
            {
                reason = "Personal email address is blank";
                return false;
            }

            if (!Enum.IsDefined (typeof (SystemChannel), applicationRequest.SystemChannel))
            {
                reason = $"System channel {applicationRequest.SystemChannel} is invalid";
                return false;
            }

            if (!string.IsNullOrWhiteSpace (applicationRequest.PurposeOfLoan))
            {
                var purposeOfLoanExists = LookupService.GetLookupEntry ("application-purposeOfLoan", applicationRequest.PurposeOfLoan);
                if (purposeOfLoanExists == null)
                {
                    reason = $"Purpose of loan {applicationRequest.PurposeOfLoan} is invalid";
                    return isValid = false;
                }
            }

            if (!string.IsNullOrWhiteSpace (applicationRequest.ResidenceType))
            {
                var residenceTypeExists = LookupService.GetLookupEntry ("application-residenceType", applicationRequest.ResidenceType);
                if (residenceTypeExists == null)
                {
                    reason = $"Residence type {applicationRequest.ResidenceType} is invalid";
                    return isValid = false;
                }
            }

            if (!Enum.IsDefined (typeof (SourceType), applicationRequest.SourceType))
            {
                reason = $"Source type {applicationRequest.SourceType} is invalid";
                return isValid = false;
            }

            if (!(applicationRequest.SourceType == SourceType.Organic) && string.IsNullOrEmpty (applicationRequest.SourceReferenceId))
            {
                reason = $"SourceReferenceId can not be blank for {applicationRequest.SourceType}";
                return isValid = false;
            }

            if (string.IsNullOrEmpty (applicationRequest.FirstName))
            {
                reason = "FirstName is blank";
                return false;
            }

            if (string.IsNullOrEmpty (applicationRequest.LastName))
            {
                reason = "LastName is blank";
                return false;
            }

            if (!Enum.IsDefined (typeof (Applicant.Gender), applicationRequest.Gender))
            {
                reason = $"Gender {applicationRequest.Gender} is invalid";
                return isValid = false;
            }

            if (!string.IsNullOrWhiteSpace (applicationRequest.AadhaarNumber))
            {
                if (applicationRequest.AadhaarNumber.Length != 12)
                {
                    reason = $"Aadhaar number length is invalid";
                    return isValid = false;
                }
            }
            // residential address validation can not be null
            // can not be both null
            // can not be null or zero year if month is null or zero
            if (applicationRequest.CurrentAddressYear == "0")
            {
                reason = "Current Residential Address Year can not be zero";
                return false;
            }

            //employement month and year validation can not be zero

            if (applicationRequest.EmployementYear == "0")
            {
                reason = "Employement Year can not be zero";
                return false;
            }
            return true;
        }
        private bool EnsureInputIsValidfForUpdateLead (ISubmitApplicationRequest applicationRequest, out string reason)
        {
            var isValid = true;
            reason = string.Empty;
            if (applicationRequest == null)
                throw new ArgumentNullException (nameof (applicationRequest), "application cannot be empty");

            if (!string.IsNullOrWhiteSpace (applicationRequest.PurposeOfLoan))
            {
                var purposeOfLoanExists = LookupService.GetLookupEntry ("application-purposeOfLoan", applicationRequest.PurposeOfLoan);
                if (purposeOfLoanExists == null)
                {
                    reason = $"Purpose of loan {applicationRequest.PurposeOfLoan} is invalid";
                    return isValid = false;
                }
            }

            if (!string.IsNullOrWhiteSpace (applicationRequest.ResidenceType))
            {
                var residenceTypeExists = LookupService.GetLookupEntry ("application-residenceType", applicationRequest.ResidenceType);
                if (residenceTypeExists == null)
                {
                    reason = $"Residence type {applicationRequest.ResidenceType} is invalid";
                    return isValid = false;
                }
            }

            if (!Enum.IsDefined (typeof (SourceType), applicationRequest.SourceType))
            {
                reason = $"Source type {applicationRequest.SourceType} is invalid";
                return isValid = false;
            }

            if (!(applicationRequest.SourceType == SourceType.Organic) && string.IsNullOrEmpty (applicationRequest.SourceReferenceId))
            {
                reason = $"SourceReferenceId can not be blank for {applicationRequest.SourceType}";
                return isValid = false;
            }

            if (!string.IsNullOrWhiteSpace (applicationRequest.AadhaarNumber))
            {
                if (applicationRequest.AadhaarNumber.Length != 12)
                {
                    reason = $"Aadhaar number length is invalid";
                    return isValid = false;
                }
            }
            // residential address validation can not be zero

            if (applicationRequest.CurrentAddressYear == "0")
            {
                reason = "Current Residential Address Year can not be zero";
                return false;
            }

            //employement month and year validation can not be zero

            if (applicationRequest.EmployementYear == "0")
            {
                reason = "Employement Year can not be zero";
                return false;
            }
            return true;
        }

        public async Task<IBankSourceData> GetBankSourceData (string applicationNumber)
        {
            var result = string.Empty;
            var sourceType = string.Empty;
            object BankReport = null;
            if (string.IsNullOrWhiteSpace (applicationNumber))
                throw new ArgumentException ($"{nameof(applicationNumber)} is mandatory");
            var bankSource = await DataAttributesEngine.GetAttribute ("application", applicationNumber, "bankPreferenceData");
            if (bankSource != null)
            {
                result = GetBankValue (bankSource);
            }
            if (!string.IsNullOrEmpty (result))
            {
                if (result == "manual")
                    sourceType = "manualCashFlowReport";
                else if (result == "yodlee")
                    sourceType = "yodleeCashFlowReport";
                else
                    sourceType = "perfiosReport";

                BankReport = await DataAttributesEngine.GetAttribute ("application", applicationNumber, sourceType);
            }
            else
            {
                var yodleeReport = await DataAttributesEngine.GetAttribute ("application", applicationNumber, "yodleeCashFlowReport");
                if (yodleeReport != null)
                {
                    sourceType = "yodleeCashFlowReport";
                    BankReport = yodleeReport;
                }
                else
                {

                    var perfiosReport = await DataAttributesEngine.GetAttribute ("application", applicationNumber, "perfiosReport");
                    if (perfiosReport != null)
                    {
                        sourceType = "perfiosReport";
                        BankReport = perfiosReport;
                    }
                    else
                    {
                        sourceType = "manualCashFlowReport";
                        var manualReport = await DataAttributesEngine.GetAttribute ("application", applicationNumber, sourceType);
                        BankReport = manualReport;
                    }

                }
            }
            return new BankSourceData () { BankReport = BankReport, BankSourceType = sourceType };
        }

        public async Task<object> UpdatePerfiosData (string applicationNumber, object cashFlowReportData)
        {
            if (string.IsNullOrWhiteSpace (applicationNumber))
                throw new ArgumentException ($"{nameof(applicationNumber)} is mandatory");

            if (cashFlowReportData == null)
                throw new ArgumentException ($"{nameof(cashFlowReportData)} is mandatory");

            await DataAttributesEngine.SetAttribute ("application", applicationNumber, "manualCashFlowReport",
                cashFlowReportData);

            await UpdateBankSourceData (applicationNumber, "manual");

            return cashFlowReportData;
        }

        public async Task<string> UpdateBankSourceData (string applicationNumber, string sourceType)
        {
            if (string.IsNullOrWhiteSpace (applicationNumber))
                throw new ArgumentException ($"{nameof(applicationNumber)} is mandatory");

            if (sourceType == null)
                throw new ArgumentException ($"{nameof(sourceType)} is mandatory");

            //var bankSource = LookupService.GetLookupEntry("application-bankSource", sourceType);
            //if (bankSource == null)
            //    throw new InvalidArgumentException("bankPreferenceSource is not valid");

            await DataAttributesEngine.SetAttribute ("application", applicationNumber, "bankPreferenceData",
                new { BankPreferenceSource = sourceType });

            return sourceType;
        }

        public async Task<bool> CheckIfDuplicateApplicationExists (UniqueParameterTypes parameterName, string parameterValue, string applicationNumber)
        {
            var result = false;
            var applications = await ApplicationFilterService.DuplicateExistsData (parameterName, parameterValue);
            if (applications != null && applications.Count () > 0)
            {
                var appsWithValidTermiantationStatus = applications.Where (x => ApplicationProcessorConfiguration.ReApplyStatuseCodes.Contains (x.StatusCode));
                var latestDuplicate = (applications != null && applications.Count () > 0) ? applications.First () : null;

                if (latestDuplicate != null &&
                    !string.IsNullOrWhiteSpace (applicationNumber) &&
                    latestDuplicate.ApplicationNumber == applicationNumber &&
                    (string.IsNullOrEmpty (latestDuplicate.StatusCode) ||
                        latestDuplicate.StatusCode == ApplicationProcessorConfiguration.Statuses["LeadCreated"] ||
                        ApplicationProcessorConfiguration.RestartApplyStatusCodes.Contains (latestDuplicate.StatusCode)))
                {
                    return result;
                }

                if (latestDuplicate.StatusCode == ApplicationProcessorConfiguration.Statuses["Rejected"] &&
                    DateTimeOffset.Now.Subtract (latestDuplicate.Submitted).Days <= ApplicationProcessorConfiguration.ReApplyTimeFrame)
                {
                    var applicant = await ApplicantService.Get (latestDuplicate.ApplicantId);
                    if (applicant.CanReapplyBy != null && applicant.CanReapplyBy.Value.Date.CompareTo (DateTime.Today) >= 0)
                    {
                        result = false;
                    }
                    else
                    {
                        result = true;
                    }
                }
                else if (ApplicationProcessorConfiguration.ReApplyStatuseCodes.Contains (latestDuplicate.StatusCode))
                {
                    result = false;
                }
                else
                {
                    result = true;
                }
            }
            else
            {
                result = false;
            }

            return result;
        }

        public async Task<IUploadStatementResponse> UploadBankStatement (string entityType, string entityId, string fileName, byte[] fileBytes, string password)
        {

            var result = await PerfiosService.UploadStatement (entityType, entityId, fileBytes, password);
            if (result != null)
            {
                var generatedDocument = await ApplicationDocumentService.Add (entityId, "bankstatement", fileBytes, fileName, null);
                if (generatedDocument != null)
                {
                    await EventHub.Publish (new BankStatementUploaded ()
                    {
                        EntityId = entityId,
                            EntityType = entityType,
                            Response = generatedDocument,
                            Request = new
                            {
                                entityId = entityId,
                                    category = "bankstatement",
                                    fileName = fileName
                            },
                            ReferenceNumber = result.ReferenceNumber,

                    });
                }
            }

            return result;
        }

        public async Task<string> UpdateStatus (string applicationNumber)
        {
            var status = EntityStatusService.GetStatusByEntity ("application", applicationNumber).Result;

            if (ApplicationProcessorConfiguration.Statuses["LeadCreated"] != status.Code)
            {
                throw new InvalidOperationException ($"Can not update status to application submitted when current status is {status.Code}");
            }

            var application = await ExternalApplicationService.GetByApplicationNumber (applicationNumber);
            if (application == null)
                throw new NotFoundException ($"Application with number {applicationNumber} is not found");

            var applicant = await ApplicantService.Get (application.ApplicantId);
            if (applicant == null)
                throw new NotFoundException ($"Applicant with id {application.ApplicantId} is not found");

            var extentionData = await DataAttributesEngine.GetAttribute ("application", applicationNumber, "extension");
            if (extentionData == null)
                throw new NotFoundException ($"Extention data for {applicationNumber} is not found");

            var applicationExtension = JObject.FromObject (extentionData).ToObject<ApplicationExtension> ();

            EnsureInputIsValid (application, applicant, applicationExtension);

            await EntityStatusService.ChangeStatus ("application", application.ApplicationNumber, ApplicationProcessorConfiguration.Statuses["ApplicationSubmitted"], null);
            return ApplicationProcessorConfiguration.Statuses["ApplicationSubmitted"];

        }

        public async Task EnableReApplyInCoolOff (string applicationNumber, string applicantId)
        {
            if (string.IsNullOrEmpty (applicationNumber))
                throw new ArgumentNullException (nameof (applicationNumber));

            if (string.IsNullOrEmpty (applicantId))
                throw new ArgumentNullException (nameof (applicantId));

            await ApplicantService.EnableReapplyInCoolOff (applicantId);
            await EventHub.Publish ("ReApplyInCoolOffEnabled", new
            {
                ApplicationNumber = applicationNumber
            });
        }

        public async Task UpdateLoanAmount (string applicationNumber, IUpdateLoanAmountRequest updateLoanAmountRequest)
        {
            if (string.IsNullOrEmpty (applicationNumber))
                throw new ArgumentNullException (nameof (applicationNumber));

            if (updateLoanAmountRequest == null)
                throw new ArgumentNullException (nameof (updateLoanAmountRequest));
            var status = EntityStatusService.GetStatusByEntity ("application", applicationNumber).Result;
            if (ApplicationProcessorConfiguration.Statuses["LeadCreated"] != status.Code)
            {
                throw new InvalidOperationException ($"Can not update Loan Amount when status is {status.Code}");
            }

            await Task.Run (() => ExternalApplicationService.UpdateLoanAmount (applicationNumber, updateLoanAmountRequest));

        }

        public async Task UpdateLoanPurpose (string applicationNumber, string loanPurpose, string otherLoanPurpose)
        {
            if (string.IsNullOrEmpty (applicationNumber))
                throw new ArgumentNullException (nameof (applicationNumber));

            if (string.IsNullOrEmpty (loanPurpose))
                throw new ArgumentNullException (nameof (loanPurpose));

            await Task.Run (() => ExternalApplicationService.UpdateLoanPurpose (applicationNumber, loanPurpose, otherLoanPurpose));
        }
        public async Task<bool> IsValidEmail (string Email)
        {
            if (string.IsNullOrEmpty (Email))
                throw new InvalidArgumentException ("Email is blank or empty");

            var blacklistedEmail = ApplicationProcessorConfiguration.EmailBlackList;
            if (blacklistedEmail != null)
            {
                foreach (string x in blacklistedEmail)
                {
                    if (Email.StartsWith (x))
                        return await Task.Run (() => false);
                }
            }

            return true;
        }

        private void EnsureInputIsValid (string applicationNumber, IEmploymentDetail employmentInformationRequest, string employementYear = null, string employementMonth = null)
        {
            if (string.IsNullOrWhiteSpace (applicationNumber))
                throw new ArgumentNullException (nameof (applicationNumber));

            if (employmentInformationRequest == null)
                throw new ArgumentNullException (nameof (employmentInformationRequest));

            if (string.IsNullOrWhiteSpace (employmentInformationRequest.Name))
                throw new ArgumentNullException ($"{nameof(employmentInformationRequest.Name)} is mandatory");

            //if (string.IsNullOrWhiteSpace(employmentInformationRequest.Designation))
            //    throw new ArgumentNullException($"{nameof(employmentInformationRequest.Designation)} is mandatory");

            //if (employmentInformationRequest.Addresses == null)
            //    throw new ArgumentNullException($"{nameof(employmentInformationRequest.Addresses)} is mandatory");

            //if (employmentInformationRequest.WorkPhoneNumbers == null)
            //    throw new ArgumentNullException($"{nameof(employmentInformationRequest.WorkPhoneNumbers)} is mandatory");

            if (string.IsNullOrEmpty (employmentInformationRequest.WorkEmail))
                throw new ArgumentNullException ($"{nameof(employmentInformationRequest.WorkEmail)} is mandatory");

            //if (employmentInformationRequest.LengthOfEmploymentInMonths <= 0)
            //    throw new ArgumentNullException($"{nameof(employmentInformationRequest.LengthOfEmploymentInMonths)} is mandatory");

            //if (string.IsNullOrEmpty(employmentInformationRequest.CinNumber))
            //    throw new ArgumentNullException($"{nameof(employmentInformationRequest.CinNumber)} is mandatory");

            if (!Enum.IsDefined (typeof (EmploymentStatus), employmentInformationRequest.EmploymentStatus))
                throw new InvalidEnumArgumentException ($"{nameof(employmentInformationRequest.EmploymentStatus)} is not valid");

            //if (employmentInformationRequest.Addresses != null)
            //{
            //    employmentInformationRequest.Addresses.ForEach(address =>
            //    {
            //        if (string.IsNullOrWhiteSpace(address.AddressLine1))
            //            throw new ArgumentException("AddressLine1 is mandatory for Work address");

            //        if (string.IsNullOrWhiteSpace(address.City))
            //            throw new InvalidArgumentException($"Work address {nameof(address.City)} is mandatory");

            //        if (string.IsNullOrWhiteSpace(address.State))
            //            throw new InvalidArgumentException($"Work address {nameof(address.State)} is mandatory");

            //        if (string.IsNullOrWhiteSpace(address.PinCode))
            //            throw new InvalidArgumentException($"Work address {nameof(address.PinCode)} is mandatory");

            //        if (string.IsNullOrWhiteSpace(address.Country))
            //            throw new InvalidArgumentException($"Work address {nameof(address.Country)} is mandatory");

            //        //if (string.IsNullOrWhiteSpace(address.AddressType.ToString()))
            //        address.AddressType = AddressType.Work;
            //        //else
            //        //{
            //        //    if (!Enum.IsDefined(typeof(AddressType), address.AddressType))
            //        //        throw new InvalidEnumArgumentException("AddressType is not valid");
            //        //}
            //    });
            //}

            //if (employmentInformationRequest.WorkPhoneNumbers != null)
            //{
            //    employmentInformationRequest.WorkPhoneNumbers.ForEach(phone =>
            //    {
            //        if (string.IsNullOrWhiteSpace(phone.Phone))
            //            throw new InvalidArgumentException($"Work phone {nameof(phone.Phone)} is mandatory");

            //        //if (string.IsNullOrWhiteSpace(phone.PhoneType.ToString()))
            //        phone.PhoneType = PhoneType.Work;
            //        //else
            //        //{
            //        //    if (!Enum.IsDefined(typeof(PhoneType), phone.PhoneType))
            //        //        throw new InvalidEnumArgumentException($"{nameof(phone.PhoneType)} is not valid");
            //        //}
            //    });
            //}
            if (employmentInformationRequest.IncomeInformation == null || employmentInformationRequest.IncomeInformation.Income == 0)
                throw new InvalidArgumentException ($"{nameof(employmentInformationRequest.IncomeInformation.Income)} is mandatory");
            //employement month and year validation
            // can not be both null
            // can not be null or zero year if month is null or zero
            if (!string.IsNullOrEmpty (employementYear))
            {
                if (employementYear == "0")
                {
                    throw new InvalidEnumArgumentException ("employement Year can not be zero");
                }
            }
        }

        private void EnsureInputIsValid (string applicationNumber, IEmailAddress emailInformationRequest)
        {
            if (string.IsNullOrWhiteSpace (applicationNumber))
                throw new ArgumentNullException (nameof (applicationNumber));

            if (emailInformationRequest == null)
                throw new ArgumentNullException (nameof (emailInformationRequest));

            if (string.IsNullOrWhiteSpace (emailInformationRequest.Email))
                throw new ArgumentNullException ($"{nameof(emailInformationRequest.Email)} is mandatory");

            //if (string.IsNullOrWhiteSpace(emailInformationRequest.EmailType.ToString()))
            emailInformationRequest.EmailType = Applicant.EmailType.Personal;
            //else
            //{
            //    if (!Enum.IsDefined(typeof(Applicant.EmailType), emailInformationRequest.EmailType))
            //        throw new InvalidEnumArgumentException($"{nameof(emailInformationRequest.EmailType)} is mandatory");
            //}
        }

        private void EnsureInputIsValid (string applicationNumber, IBankInformation bankInformationRequest)
        {
            if (string.IsNullOrWhiteSpace (applicationNumber))
                throw new ArgumentNullException (nameof (applicationNumber));

            if (bankInformationRequest == null)
                throw new ArgumentNullException (nameof (bankInformationRequest));

            if (string.IsNullOrWhiteSpace (bankInformationRequest.BankName))
                throw new ArgumentNullException ($"{nameof(bankInformationRequest.BankName)} is mandatory");

            //if (string.IsNullOrWhiteSpace(bankInformationRequest.AccountHolderName))
            //    throw new ArgumentNullException($"{nameof(bankInformationRequest.AccountHolderName)} is mandatory");

            // if (string.IsNullOrWhiteSpace(bankInformationRequest.AccountNumber))
            //   throw new ArgumentNullException($"{nameof(bankInformationRequest.AccountNumber)} is mandatory");

            if (bankInformationRequest.BankAddresses != null)
                bankInformationRequest.BankAddresses.AddressType = AddressType.Bank;
        }

        private void EnsureInputIsValid (string applicationNumber, IEducationInformation educationInformationRequest)
        {
            if (string.IsNullOrWhiteSpace (applicationNumber))
                throw new ArgumentNullException (nameof (applicationNumber));

            if (educationInformationRequest == null)
                throw new ArgumentNullException (nameof (educationInformationRequest));

            if (string.IsNullOrWhiteSpace (educationInformationRequest.EducationalInstitution))
                throw new ArgumentNullException ($"{nameof(educationInformationRequest.EducationalInstitution)} is mandatory");

            if (string.IsNullOrWhiteSpace (educationInformationRequest.LevelOfEducation))
                throw new ArgumentNullException ($"{nameof(educationInformationRequest.LevelOfEducation)} is mandatory");

            var levelOfEducationExists = LookupService.GetLookupEntry ("application-levelOfEducation", educationInformationRequest.LevelOfEducation);
            if (levelOfEducationExists == null)
                throw new InvalidArgumentException ("levelOfEducation  is not valid");

        }

        private bool EnsureInputIsValid (string applicationNumber, UpdateApplicantRequest applicantDetails)
        {
            var isValid = true;

            if (applicantDetails == null)
                throw new ArgumentNullException (nameof (applicantDetails), "Persoanl details cannot be empty");

            if (string.IsNullOrEmpty (applicantDetails.Salutation))
                return isValid = false;

            if (string.IsNullOrEmpty (applicantDetails.FirstName))
                return isValid = false;

            if (string.IsNullOrEmpty (applicantDetails.LastName))
                return isValid = false;

            if (applicantDetails.DateOfBirth == null)
                return isValid = false;

            if (!Enum.IsDefined (typeof (MaritalStatus), applicantDetails.MaritalStatus))
                return isValid = false;

            if (!Enum.IsDefined (typeof (Applicant.Gender), applicantDetails.Gender))
                return isValid = false;

            if (string.IsNullOrWhiteSpace (applicantDetails.PermanentAccountNumber))
                return isValid = false;

            if (string.IsNullOrWhiteSpace (applicantDetails.AadhaarNumber))
                return isValid = false;
            if (applicantDetails.AadhaarNumber.Length != 12)
                return isValid = false;
            return isValid;
        }

        private void EnsureInputIsValid (IApplication application, IApplicant applicant, ApplicationExtension extentionData)
        {
            var mobileNumber = applicant.PhoneNumbers.FirstOrDefault (phone => phone.PhoneType == PhoneType.Mobile);

            if (mobileNumber == null)
                throw new ArgumentNullException ("Mobile number is mandantory");

            if (string.IsNullOrWhiteSpace (mobileNumber.Phone))
                throw new ArgumentNullException ("Mobile number is mandantory");

            if (!extentionData.MobileVerificationDetail.IsVerified)
                throw new InvalidOperationException ("Mobile number is not verified");

            if (string.IsNullOrWhiteSpace (application.EmailAddress.Email))
                throw new ArgumentNullException ("Personal email id is mandantory");

            //if (ApplicationProcessorConfiguration.EnablePersonalEmailVerification)
            //{
            //    if (string.IsNullOrWhiteSpace(extentionData.EmailHunterVerificationResult) || extentionData.EmailHunterVerificationResult.ToLower() != "true")
            //    {
            //        string isEmailVerify = VerifyEmailAddressWithHunter(application.EmailAddress.Email);
            //        extentionData.EmailHunterVerificationResult = isEmailVerify;
            //        DataAttributesEngine.SetAttribute("application", application.ApplicationNumber, "extension", extentionData).Wait();
            //        if (isEmailVerify.ToLower() != "true")
            //            throw new InvalidOperationException("Email address is not verified");
            //    }
            //}

            if (application.RequestedAmount <= 0)
                throw new ArgumentNullException ($"Application {nameof(application.RequestedAmount)} is mandatory");

            if (!Enum.IsDefined (typeof (LoanFrequency), application.RequestedTermType))
                throw new InvalidEnumArgumentException (nameof (application.RequestedTermType), (int) application.RequestedTermType, typeof (LoanFrequency));

            if (application.EmploymentDetail.IncomeInformation.Income > 0 && !Enum.IsDefined (typeof (PaymentFrequency), application.EmploymentDetail.IncomeInformation.PaymentFrequency))
                throw new InvalidEnumArgumentException (nameof (application.EmploymentDetail.IncomeInformation.PaymentFrequency), (int) application.EmploymentDetail.IncomeInformation.PaymentFrequency, typeof (PaymentFrequency));

            if (!Enum.IsDefined (typeof (SystemChannel), application.Source.SystemChannel))
                throw new InvalidEnumArgumentException (nameof (application.Source.SystemChannel), (int) application.Source.SystemChannel, typeof (SystemChannel));

            if (string.IsNullOrWhiteSpace (application.PurposeOfLoan))
                throw new ArgumentNullException ("PurposeOfLoan is mandatory");

            var purposeOfLoanExists = LookupService.GetLookupEntry ("application-purposeOfLoan", application.PurposeOfLoan);
            if (purposeOfLoanExists == null)
                throw new InvalidArgumentException ("Purpose of loan is not valid");

            if (string.IsNullOrWhiteSpace (application.ResidenceType))
                throw new ArgumentNullException ("Residence Type is mandatory");

            var residenceTypeExists = LookupService.GetLookupEntry ("application-residenceType", application.ResidenceType);
            if (residenceTypeExists == null)
                throw new InvalidArgumentException ("Residence type is not valid.");

            if (application.Source == null)
                throw new ArgumentNullException ("Application source is mandatory");

            if (!Enum.IsDefined (typeof (SourceType), application.Source.SourceType))
                throw new InvalidEnumArgumentException ("Application source type is not valid");

            if (!(application.Source.SourceType == SourceType.Organic) && string.IsNullOrEmpty (application.Source.SourceReferenceId))
                throw new ArgumentNullException ("Source reference id is mandatory");

            if (string.IsNullOrEmpty (applicant.Salutation))
                throw new ArgumentNullException ("Salutation is mandatory");

            if (string.IsNullOrEmpty (applicant.FirstName))
                throw new ArgumentNullException ("FirstName is mandatory");

            if (string.IsNullOrEmpty (applicant.LastName))
                throw new ArgumentNullException ("LastName is mandatory");

            if (applicant.DateOfBirth == null)
                throw new ArgumentNullException ("DateOfBirth is mandatory");

            if (!Enum.IsDefined (typeof (MaritalStatus), applicant.MaritalStatus))
                throw new InvalidEnumArgumentException ("Marital Status is not valid");

            if (!Enum.IsDefined (typeof (Applicant.Gender), applicant.Gender))
                throw new InvalidEnumArgumentException ("Gender is not valid");

            if (string.IsNullOrWhiteSpace (applicant.PermanentAccountNumber))
                throw new ArgumentNullException ("Permanent Account Number is mandatory");

            if (string.IsNullOrWhiteSpace (applicant.AadhaarNumber))

                throw new ArgumentNullException ("AadhaarNumber Number is mandatory");

            if (applicant.AadhaarNumber.Length != 12)

                throw new ArgumentNullException ("AadhaarNumber Numbe length is invalid");

            if (applicant.Addresses == null || !applicant.Addresses.Any ())
                throw new ArgumentNullException ("Current address and permanent address is mandantory");

            var currentAddress = applicant.Addresses.FirstOrDefault (add => add.AddressType == AddressType.Current);
            if (currentAddress == null)
                throw new ArgumentNullException ("Current address is mandantory");

            if (string.IsNullOrWhiteSpace (currentAddress.AddressLine1))
                throw new ArgumentNullException ("Current address line1 is mandatory");

            if (string.IsNullOrWhiteSpace (currentAddress.City))
                throw new ArgumentNullException ("Current city is mandatory");

            if (string.IsNullOrWhiteSpace (currentAddress.State))
                throw new ArgumentNullException ("Current state is mandatory");

            if (string.IsNullOrWhiteSpace (currentAddress.PinCode))
                throw new ArgumentNullException ("Current pinCode is mandatory");

            if (string.IsNullOrWhiteSpace (currentAddress.Country))
                throw new ArgumentNullException ("Current country is mandatory");

            var permenantAddress = applicant.Addresses.FirstOrDefault (add => add.AddressType == AddressType.Permanant);
            if (permenantAddress == null)
                throw new ArgumentNullException ("Permanant address is mandantory");

            if (string.IsNullOrWhiteSpace (permenantAddress.AddressLine1))
                throw new ArgumentNullException ("Permanent address line1 is mandatory");

            if (string.IsNullOrWhiteSpace (permenantAddress.City))
                throw new ArgumentNullException ("Permanent city is mandatory");

            if (string.IsNullOrWhiteSpace (permenantAddress.State))
                throw new ArgumentNullException ("Permanent state is mandatory");

            if (string.IsNullOrWhiteSpace (permenantAddress.PinCode))
                throw new ArgumentNullException ("Permanent pinCode is mandatory");

            if (string.IsNullOrWhiteSpace (permenantAddress.Country))
                throw new ArgumentNullException ("Permanent country is mandatory");

            if (applicant.PhoneNumbers == null || !applicant.PhoneNumbers.Any ())
                throw new ArgumentNullException ("Personal phone number is mandatory");

            if (application.EmploymentDetail == null)
                throw new ArgumentNullException ("Employment detail is mandantory");

            if (string.IsNullOrWhiteSpace (application.EmploymentDetail.Name))
                throw new ArgumentNullException ("Employer name is mandatory");

            if (string.IsNullOrEmpty (application.EmploymentDetail.WorkEmail))
                throw new ArgumentNullException ("Work email is mandatory");

            if (string.IsNullOrEmpty (application.EmploymentDetail.CinNumber))
                throw new ArgumentNullException ("Cin number is mandatory");

            if (!Enum.IsDefined (typeof (EmploymentStatus), application.EmploymentDetail.EmploymentStatus))
                throw new InvalidEnumArgumentException ("Employment status is not valid");

            if (application.EmploymentDetail.IncomeInformation == null || application.EmploymentDetail.IncomeInformation.Income <= 0)
                throw new InvalidArgumentException ("Income is mandatory");
            // residential address validation can not be zero

            if (application.CurrentAddressYear == "0")
            {
                throw new InvalidArgumentException ("Current Residential Address Year can not be zero");

            }

            //employement month and year validation can not be zero

            if (application.EmployementYear == "0")
            {
                throw new InvalidArgumentException ("Employement Year Address can not be zero");
            }

        }
        private void EnsureInputIsValid (string applicationNumber, IAddress address)
        {
            if (address == null)
                throw new ArgumentNullException ("Current address is mandantory");

            if (!Enum.IsDefined (typeof (AddressType), address.AddressType))
                throw new InvalidEnumArgumentException (nameof (address.AddressType), (int) address.AddressType, typeof (AddressType));

            if (string.IsNullOrWhiteSpace (address.AddressLine1))
                throw new ArgumentNullException ("Permanent address line1 is mandatory");

            if (string.IsNullOrWhiteSpace (address.City))
                throw new ArgumentNullException ("Permanent city is mandatory");

            if (string.IsNullOrWhiteSpace (address.State))
                throw new ArgumentNullException ("Permanent state is mandatory");

            if (string.IsNullOrWhiteSpace (address.PinCode))
                throw new ArgumentNullException ("Permanent pinCode is mandatory");

            if (string.IsNullOrWhiteSpace (address.Country))
                throw new ArgumentNullException ("Permanent country is mandatory");

        }

        private async Task SwitchOffReApply (string applicationNumber, string applicantId)
        {
            if (string.IsNullOrEmpty (applicantId))
                throw new ArgumentNullException (nameof (applicantId));

            if (string.IsNullOrEmpty (applicationNumber))
                throw new ArgumentNullException (nameof (applicationNumber));

            await ApplicantService.SwitchOffReApply (applicantId);

            await EventHub.Publish ("ReApplyInCoolOffDisabled", new
            {
                ApplicationNumber = applicationNumber
            });
        }

        public async Task UpdateAadhaarnumber (string applicationNumber, string aadhaarNumber)
        {
            if (string.IsNullOrWhiteSpace (applicationNumber))
                throw new ArgumentNullException (nameof (applicationNumber));

            var application = await ExternalApplicationService.GetByApplicationNumber (applicationNumber);

            if (application == null)
                throw new NotFoundException ($"Application with number {applicationNumber} is not found");

            await ApplicantService.UpdateAadhaarNumber (application.ApplicantId, aadhaarNumber);

            var applicant = await ApplicantService.Get (application.ApplicantId);

            if (applicant == null)
                throw new NotFoundException ($"Applicant with number {application.ApplicantId} is not found");

            await EventHub.Publish (new ApplicationModified { Application = application, Applicant = applicant });

        }

        public async Task<IApplicationResponse> AddLeadForNewBorrowerPortal (ISubmitApplicationRequest submitApplicationRequest)
        {

            string reason;
            var isValid = EnsureInputIsValidForeNewLead (submitApplicationRequest, out reason);

            // Throw exception if request object is null
            if (submitApplicationRequest == null)
                throw new ArgumentNullException (nameof (submitApplicationRequest));

            // check if Personal Mobile, Personal Email and PAN number is duplicate or not

            var isExists = false;

            if (!string.IsNullOrWhiteSpace (submitApplicationRequest.PersonalMobile))
            {
                isExists = await CheckIfDuplicateApplicationExists (UniqueParameterTypes.Mobile, submitApplicationRequest.PersonalMobile, string.Empty);
                if (isExists)
                    throw new InvalidOperationException ("Application with same mobile number already exists");
            }

            if (!string.IsNullOrWhiteSpace (submitApplicationRequest.PermanentAccountNumber))
            {
                isExists = await CheckIfDuplicateApplicationExists (UniqueParameterTypes.Pan, submitApplicationRequest.PermanentAccountNumber, string.Empty);
                if (isExists)
                    throw new InvalidOperationException ("Application with same permanent account number already exists");
            }

            if (!string.IsNullOrWhiteSpace (submitApplicationRequest.PersonalEmail))
            {
                isExists = await CheckIfDuplicateApplicationExists (UniqueParameterTypes.Email, submitApplicationRequest.PersonalEmail, string.Empty);
                if (isExists)
                    throw new InvalidOperationException ("Application with same email id already exists");
            }

            // Get application request
            var applicationRequest = GetApplicationRequest (submitApplicationRequest);

            // add application with application service end point
            var application = await ExternalApplicationService.Add (applicationRequest);

            // Call verify method of Verification engine to verify mobile Verification.
            await VerificationEngineService.Verify ("application", application.ApplicationNumber, "MobileVerification",
                "MobileSMS", "MobileSMS", new { result = submitApplicationRequest.IsMobileVerified.ToString ().ToLower () });

            // Set extension Data attribute
            await DataAttributesEngine.SetAttribute ("application", application.ApplicationNumber, "extension", GetApplicationExtension (submitApplicationRequest, string.Empty, ApplicationProcessorConfiguration.EnablePersonalEmailVerification, ApplicationProcessorConfiguration.EmailValidationProvider.ToLower ()));

            // Change status of application to Lead Created State
            await EntityStatusService.ChangeStatus ("application", application.ApplicationNumber, ApplicationProcessorConfiguration.Statuses["LeadCreated"], null);

            return application;
        }
        public async Task<Tuple<IApplication, string>> UpdatePartialApplicationData (string applicationNumber, ISubmitApplicationRequest submitApplicationRequest)
        {
            Logger.Info ($"[OfferEngine/ApplicationService] UpdateApplication method execution started for [{applicationNumber}] request : [{  (submitApplicationRequest != null ? JsonConvert.SerializeObject(submitApplicationRequest) : null)}] ");
            var Message = new List<string> ();
            var activeWorkflow = await EntityStatusService.GetActiveStatusWorkFlow ("application", applicationNumber);
            var status = EntityStatusService.GetStatusByEntity ("application", applicationNumber, activeWorkflow.StatusWorkFlowId).Result;
            string reason;
            var isValid = EnsureInputIsValidForeNewLead (submitApplicationRequest, out reason);
            var isExists = false;

            if (!string.IsNullOrWhiteSpace (submitApplicationRequest.PersonalMobile))
            {
                isExists = await CheckIfDuplicateApplicationExists (UniqueParameterTypes.Mobile, submitApplicationRequest.PersonalMobile, applicationNumber).ConfigureAwait (false);
                if (isExists)
                    throw new InvalidOperationException ("Application with same mobile number already exists");
            }

            if (!string.IsNullOrWhiteSpace (submitApplicationRequest.PermanentAccountNumber))
            {
                isExists = await CheckIfDuplicateApplicationExists (UniqueParameterTypes.Pan, submitApplicationRequest.PermanentAccountNumber, applicationNumber).ConfigureAwait (false);
                if (isExists)
                    throw new InvalidOperationException ("Application with same permanent account number already exists");
            }

            if (!string.IsNullOrWhiteSpace (submitApplicationRequest.PersonalEmail))
            {
                isExists = await CheckIfDuplicateApplicationExists (
                    UniqueParameterTypes.Email,
                    submitApplicationRequest.PersonalEmail,
                    applicationNumber).ConfigureAwait (false);
                if (isExists) throw new InvalidOperationException ("Application with same email id already exists");
            }

            var emailVerifyResponse = "true";
            var applicationResponse = await ExternalApplicationService.GetByApplicationNumber (applicationNumber);
            if (applicationResponse == null)
            {
                throw new NotFoundException ($"Application with number {applicationNumber} is not found");
            }
            var applicantResponse = await ApplicantService.Get (applicationResponse.ApplicantId);
            if (applicantResponse == null)
            {
                throw new NotFoundException ($"Application with number {applicationResponse.ApplicantId} is not found");
            }

            var updateApplicationRequest = new SubmitApplicationRequest ();
            updateApplicationRequest.PurposeOfLoan = AssignWisely (submitApplicationRequest.PurposeOfLoan, applicationResponse.PurposeOfLoan);
            updateApplicationRequest.RequestedAmount = AssignWisely (submitApplicationRequest.RequestedAmount, applicationResponse.RequestedAmount); //
            updateApplicationRequest.RequestedTermType = AssignWisely (submitApplicationRequest.RequestedTermType, applicationResponse.RequestedTermType);
            updateApplicationRequest.RequestedTermValue = AssignWisely (submitApplicationRequest.RequestedTermValue, applicationResponse.RequestedTermValue);

            // setting year and month of residence address and employement

            // if (!string.IsNullOrEmpty (submitApplicationRequest.CurrentAddressYear)) {

            if (submitApplicationRequest.CurrentAddressYear == "0")
            {
                Message.Add ("Current Residential Address Year can not be zero");

            }
            //  updateApplicationRequest.CurrentAddressMonth = AssignWisely (submitApplicationRequest.CurrentAddressMonth, applicationResponse.CurrentAddressMonth);
            updateApplicationRequest.CurrentAddressYear = AssignWisely (submitApplicationRequest.CurrentAddressYear, applicationResponse.CurrentAddressYear);

            //}

            // if (!string.IsNullOrEmpty (submitApplicationRequest.EmployementYear)) {
            if (submitApplicationRequest.EmployementYear == "0")
            {
                Message.Add ("Employement Year Year can not be zero");

            }
            // updateApplicationRequest.EmployementMonth = AssignWisely (submitApplicationRequest.EmployementMonth, applicationResponse.EmployementMonth);
            updateApplicationRequest.EmployementYear = AssignWisely (submitApplicationRequest.EmployementYear, applicationResponse.EmployementYear);
            // }

            //personal Details

            var PerDetailsFlag = true;
            if (status != null &&
                ApplicationProcessorConfiguration != null &&
                !(ApplicationProcessorConfiguration.UpdatePersonalDetails.Contains (status.Code)) &&
                (
                    (!CheckNullOrEmpty (submitApplicationRequest.AadhaarNumber)) ||
                    (!CheckNullOrEmpty (submitApplicationRequest.DateOfBirth)) ||
                    (!CheckNullOrEmpty (submitApplicationRequest.Salutation)) ||
                    (!CheckNullOrEmpty (submitApplicationRequest.FirstName)) ||
                    (!CheckNullOrEmpty (submitApplicationRequest.MiddleName)) ||
                    (!CheckNullOrEmpty (submitApplicationRequest.LastName)) ||
                    (!CheckNullOrEmpty (submitApplicationRequest.Gender)) ||
                    (!CheckNullOrEmpty (submitApplicationRequest.MaritalStatus)) ||
                    (!CheckNullOrEmpty (submitApplicationRequest.PermanentAccountNumber)) ||
                    (!CheckNullOrEmpty (submitApplicationRequest.PersonalMobile)) ||
                    (!CheckNullOrEmpty (submitApplicationRequest.PersonalEmail)) ||
                    (!CheckNullOrEmpty (submitApplicationRequest.ResidenceType))
                )
            )
            {
                PerDetailsFlag = false;
                Message.Add ($"Can not update Personal Details when status is {status.Code}");
            }

            updateApplicationRequest.AadhaarNumber = PerDetailsFlag == false ? applicantResponse.AadhaarNumber : AssignWisely (submitApplicationRequest.AadhaarNumber, applicantResponse.AadhaarNumber);
            updateApplicationRequest.DateOfBirth = PerDetailsFlag == false ? applicantResponse.DateOfBirth : AssignWisely (submitApplicationRequest.DateOfBirth, applicantResponse.DateOfBirth);
            updateApplicationRequest.Salutation = PerDetailsFlag == false ? applicantResponse.Salutation : AssignWisely (submitApplicationRequest.Salutation, applicantResponse.Salutation);
            updateApplicationRequest.FirstName = PerDetailsFlag == false ? applicantResponse.FirstName : AssignWisely (submitApplicationRequest.FirstName, applicantResponse.FirstName);
            updateApplicationRequest.MiddleName = PerDetailsFlag == false ? applicantResponse.MiddleName : AssignWisely (submitApplicationRequest.MiddleName, applicantResponse.MiddleName);
            updateApplicationRequest.LastName = PerDetailsFlag == false ? applicantResponse.LastName : AssignWisely (submitApplicationRequest.LastName, applicantResponse.LastName);
            updateApplicationRequest.Gender = PerDetailsFlag == false ? applicantResponse.Gender : AssignWisely (submitApplicationRequest.Gender, applicantResponse.Gender);
            updateApplicationRequest.MaritalStatus = PerDetailsFlag == false ? applicantResponse.MaritalStatus : AssignWisely (submitApplicationRequest.MaritalStatus, applicantResponse.MaritalStatus);

            updateApplicationRequest.PermanentAccountNumber = PerDetailsFlag == false ? applicantResponse.PermanentAccountNumber : AssignWisely (submitApplicationRequest.PermanentAccountNumber, applicantResponse.PermanentAccountNumber);
            updateApplicationRequest.PersonalMobile = PerDetailsFlag == false ? applicantResponse.PhoneNumbers.Where (x => x.PhoneType == PhoneType.Mobile).FirstOrDefault ().Phone : AssignWisely (submitApplicationRequest.PersonalMobile, applicantResponse.PhoneNumbers.Where (x => x.PhoneType == PhoneType.Mobile).FirstOrDefault ().Phone);
            updateApplicationRequest.IsMobileVerified = true;

            updateApplicationRequest.PersonalEmail = PerDetailsFlag == false ? applicantResponse.EmailAddresses.Where (x => x.EmailType == EmailType.Personal).FirstOrDefault ().Email : AssignWisely (submitApplicationRequest.PersonalEmail, applicantResponse.EmailAddresses.Where (x => x.EmailType == EmailType.Personal).FirstOrDefault ().Email);
            updateApplicationRequest.ResidenceType = PerDetailsFlag == false ? applicationResponse.ResidenceType : AssignWisely (submitApplicationRequest.ResidenceType, applicationResponse.ResidenceType);

            //can update on any of the status.
            updateApplicationRequest.EducationalInstitution = AssignWisely (submitApplicationRequest.EducationalInstitution, applicantResponse.HighestEducationInformation.EducationalInstitution);
            updateApplicationRequest.LevelOfEducation = AssignWisely (submitApplicationRequest.LevelOfEducation, applicantResponse.HighestEducationInformation.LevelOfEducation);

            //Addresschange Status
            var CurAddress = applicantResponse.Addresses.Where (x => x.AddressType == AddressType.Current).FirstOrDefault ();
            var addressFlag = true;
            if (status != null &&
                ApplicationProcessorConfiguration != null &&
                !(ApplicationProcessorConfiguration.AddressChangeStatus.Contains (status.Code)) &&
                (!CheckNullOrEmpty (submitApplicationRequest.CurrentAddressLine1) ||
                    !CheckNullOrEmpty (submitApplicationRequest.CurrentAddressLine2) ||
                    !CheckNullOrEmpty (submitApplicationRequest.CurrentCity) ||
                    !CheckNullOrEmpty (submitApplicationRequest.CurrentLandMark) ||
                    !CheckNullOrEmpty (submitApplicationRequest.CurrentLocation) ||
                    !CheckNullOrEmpty (submitApplicationRequest.CurrentPinCode) ||
                    !CheckNullOrEmpty (submitApplicationRequest.CurrentState)
                )
            )
            {
                addressFlag = false;
                Message.Add ($"Can not update current address detail when status is {status.Code}");
            }

            updateApplicationRequest.CurrentAddressLine1 = addressFlag == false? CurAddress.AddressLine1 : AssignWisely (submitApplicationRequest.CurrentAddressLine1, CurAddress.AddressLine1);
            updateApplicationRequest.CurrentAddressLine2 = addressFlag == false? CurAddress.AddressLine2 : AssignWisely (submitApplicationRequest.CurrentAddressLine2, CurAddress.AddressLine2);
            updateApplicationRequest.CurrentAddressLine3 = addressFlag == false? CurAddress.AddressLine3 : AssignWisely (submitApplicationRequest.CurrentAddressLine3, CurAddress.AddressLine3);
            updateApplicationRequest.CurrentAddressLine4 = addressFlag == false? CurAddress.AddressLine4 : AssignWisely (submitApplicationRequest.CurrentAddressLine4, CurAddress.AddressLine4);
            updateApplicationRequest.CurrentCity = addressFlag == false? CurAddress.City : AssignWisely (submitApplicationRequest.CurrentCity, CurAddress.City);
            updateApplicationRequest.CurrentCountry = "India"; //AssignWisely (submitApplicationRequest.CurrentCountry, CurAddress.Country);
            updateApplicationRequest.CurrentLandMark = addressFlag == false? CurAddress.LandMark : AssignWisely (submitApplicationRequest.CurrentLandMark, CurAddress.LandMark);
            updateApplicationRequest.CurrentLocation = addressFlag == false? CurAddress.Location : AssignWisely (submitApplicationRequest.CurrentLocation, CurAddress.Location);
            updateApplicationRequest.CurrentPinCode = addressFlag == false? CurAddress.PinCode : AssignWisely (submitApplicationRequest.CurrentPinCode, CurAddress.PinCode);
            updateApplicationRequest.CurrentState = addressFlag == false? CurAddress.State : AssignWisely (submitApplicationRequest.CurrentState, CurAddress.State);

            //permenant address can be update on any of the status.
            var perAddress = applicantResponse.Addresses.Where (x => x.AddressType == AddressType.Permanant).FirstOrDefault ();
            updateApplicationRequest.PermanentAddressLine1 = AssignWisely (submitApplicationRequest.PermanentAddressLine1, perAddress.AddressLine1);
            updateApplicationRequest.PermanentAddressLine2 = AssignWisely (submitApplicationRequest.PermanentAddressLine2, perAddress.AddressLine2);
            updateApplicationRequest.PermanentAddressLine3 = AssignWisely (submitApplicationRequest.PermanentAddressLine3, perAddress.AddressLine3);
            updateApplicationRequest.PermanentAddressLine4 = AssignWisely (submitApplicationRequest.PermanentAddressLine4, perAddress.AddressLine4);
            updateApplicationRequest.PermanentCity = AssignWisely (submitApplicationRequest.PermanentCity, perAddress.City);
            updateApplicationRequest.PermanentCountry = "India"; //AssignWisely (submitApplicationRequest.PermanentCountry, perAddress.Country);
            updateApplicationRequest.PermanenttLandMark = AssignWisely (submitApplicationRequest.PermanenttLandMark, perAddress.LandMark);
            updateApplicationRequest.PermanentLocation = AssignWisely (submitApplicationRequest.PermanentLocation, perAddress.Location);
            updateApplicationRequest.PermanentPinCode = AssignWisely (submitApplicationRequest.PermanentPinCode, perAddress.PinCode);
            updateApplicationRequest.PermanentState = AssignWisely (submitApplicationRequest.PermanentState, perAddress.State);
            //Address change status

            //employee Details update

            var EmployeeFlag = true;
            if (status != null &&
                ApplicationProcessorConfiguration != null &&
                !(ApplicationProcessorConfiguration.EmploymentChangeStatus.Contains (status.Code)) &&
                (
                    (!CheckNullOrEmpty (submitApplicationRequest.CinNumber)) ||
                    (!CheckNullOrEmpty (submitApplicationRequest.EmploymentStatus)) ||
                    (!CheckNullOrEmpty (submitApplicationRequest.LengthOfEmploymentInMonths)) ||
                    (!CheckNullOrEmpty (submitApplicationRequest.EmployerName)) ||
                    (!CheckNullOrEmpty (submitApplicationRequest.WorkEmail)) ||
                    (!CheckNullOrEmpty (submitApplicationRequest.Income)) ||
                    (!CheckNullOrEmpty (submitApplicationRequest.PaymentFrequency)) ||
                    (!CheckNullOrEmpty (submitApplicationRequest.WorkMobile))
                )
            )
            {
                EmployeeFlag = false;
                Message.Add ($"Can not update Employment detail when status is {status.Code}");
            }
            updateApplicationRequest.EmploymentAsOfDate = (AssignWisely (submitApplicationRequest.EmploymentAsOfDate, applicantResponse.EmploymentDetails.LastOrDefault ().AsOfDate));
            updateApplicationRequest.CinNumber = EmployeeFlag == false ? applicantResponse.EmploymentDetails.LastOrDefault ().CinNumber : (AssignWisely (submitApplicationRequest.CinNumber, applicantResponse.EmploymentDetails.LastOrDefault ().CinNumber));
            updateApplicationRequest.Designation = (AssignWisely (submitApplicationRequest.Designation, applicantResponse.EmploymentDetails.LastOrDefault ().Designation));
            updateApplicationRequest.EmploymentStatus = EmployeeFlag == false ? applicantResponse.EmploymentDetails.LastOrDefault ().EmploymentStatus : (AssignWisely (submitApplicationRequest.EmploymentStatus, applicantResponse.EmploymentDetails.LastOrDefault ().EmploymentStatus));
            updateApplicationRequest.LengthOfEmploymentInMonths = EmployeeFlag == false ? applicantResponse.EmploymentDetails.LastOrDefault ().LengthOfEmploymentInMonths : (AssignWisely (submitApplicationRequest.LengthOfEmploymentInMonths, applicantResponse.EmploymentDetails.LastOrDefault ().LengthOfEmploymentInMonths));
            updateApplicationRequest.EmployerName = EmployeeFlag == false ? applicantResponse.EmploymentDetails.LastOrDefault ().Name : (AssignWisely (submitApplicationRequest.EmployerName, applicantResponse.EmploymentDetails.LastOrDefault ().Name));
            updateApplicationRequest.WorkEmail = EmployeeFlag == false ? applicantResponse.EmploymentDetails.LastOrDefault ().WorkEmail : (AssignWisely (submitApplicationRequest.WorkEmail, applicantResponse.EmploymentDetails.LastOrDefault ().WorkEmail));
            updateApplicationRequest.Income = EmployeeFlag == false ? applicantResponse.EmploymentDetails.LastOrDefault ().IncomeInformation.Income : (AssignWisely (submitApplicationRequest.Income, applicantResponse.EmploymentDetails.LastOrDefault ().IncomeInformation.Income));
            updateApplicationRequest.PaymentFrequency = EmployeeFlag == false ? applicantResponse.EmploymentDetails.LastOrDefault ().IncomeInformation.PaymentFrequency : (AssignWisely (submitApplicationRequest.PaymentFrequency, applicantResponse.EmploymentDetails.LastOrDefault ().IncomeInformation.PaymentFrequency));
            var phone = "";
            if (applicantResponse.EmploymentDetails.LastOrDefault ().WorkPhoneNumbers != null)
            {
                phone = applicantResponse.EmploymentDetails.LastOrDefault ().WorkPhoneNumbers.Where (x => x.PhoneType == PhoneType.Work).FirstOrDefault ().Phone;
            }
            var EmpAddress = applicantResponse.EmploymentDetails.LastOrDefault ().Addresses.Where (x => x.AddressType == AddressType.Work).FirstOrDefault ();
            updateApplicationRequest.WorkMobile = EmployeeFlag == false ? phone : (AssignWisely (submitApplicationRequest.WorkMobile, phone));

            //EmployeeFlag address can be update in any of the stage.
            updateApplicationRequest.WorkAddressLine1 = (AssignWisely (submitApplicationRequest.WorkAddressLine1, EmpAddress.AddressLine1));
            updateApplicationRequest.WorkAddressLine2 = (AssignWisely (submitApplicationRequest.WorkAddressLine2, EmpAddress.AddressLine2));
            updateApplicationRequest.WorkAddressLine3 = (AssignWisely (submitApplicationRequest.WorkAddressLine3, EmpAddress.AddressLine3));
            updateApplicationRequest.WorkAddressLine4 = (AssignWisely (submitApplicationRequest.WorkAddressLine4, EmpAddress.AddressLine4));
            updateApplicationRequest.WorkCity = (AssignWisely (submitApplicationRequest.WorkCity, EmpAddress.City));
            updateApplicationRequest.WorkCountry = (AssignWisely (submitApplicationRequest.WorkCountry, EmpAddress.Country));
            updateApplicationRequest.WorkLandMark = (AssignWisely (submitApplicationRequest.WorkLandMark, EmpAddress.LandMark));
            updateApplicationRequest.WorkLocation = (AssignWisely (submitApplicationRequest.WorkLocation, EmpAddress.Location));
            updateApplicationRequest.WorkPinCode = (AssignWisely (submitApplicationRequest.WorkPinCode, EmpAddress.PinCode));
            updateApplicationRequest.WorkState = (AssignWisely (submitApplicationRequest.WorkState, EmpAddress.State));

            //employee Details ends here...

            //Self Declaration
            var SelfDecFlag = true;
            if (status != null &&
                ApplicationProcessorConfiguration != null &&
                !(ApplicationProcessorConfiguration.ExpenseChangeStatus.Contains (status.Code)) &&
                (
                    (!CheckNullOrEmpty (submitApplicationRequest.CreditCardBalances)) ||
                    (!CheckNullOrEmpty (submitApplicationRequest.DebtPayments)) ||
                    (!CheckNullOrEmpty (submitApplicationRequest.MonthlyExpenses)) ||
                    (!CheckNullOrEmpty (submitApplicationRequest.MonthlyRent))
                )
            )
            {
                SelfDecFlag = false;
                Message.Add ($"Can not update Expense Details detail when status is {status.Code}");

            }
            updateApplicationRequest.CreditCardBalances = SelfDecFlag == false ? applicationResponse.SelfDeclareExpense.CreditCardBalances : (AssignWisely (submitApplicationRequest.CreditCardBalances, applicationResponse.SelfDeclareExpense.CreditCardBalances));
            updateApplicationRequest.DebtPayments = SelfDecFlag == false ? applicationResponse.SelfDeclareExpense.DebtPayments : (AssignWisely (submitApplicationRequest.DebtPayments, applicationResponse.SelfDeclareExpense.DebtPayments));
            updateApplicationRequest.MonthlyExpenses = SelfDecFlag == false ? applicationResponse.SelfDeclareExpense.MonthlyExpenses : (AssignWisely (submitApplicationRequest.MonthlyExpenses, applicationResponse.SelfDeclareExpense.MonthlyExpenses));
            updateApplicationRequest.MonthlyRent = SelfDecFlag == false ? applicationResponse.SelfDeclareExpense.MonthlyRent : (AssignWisely (submitApplicationRequest.MonthlyRent, applicationResponse.SelfDeclareExpense.MonthlyRent));
            //selfDeclaration

            updateApplicationRequest.SourceReferenceId = (AssignWisely (submitApplicationRequest.SourceReferenceId, applicationResponse.Source.SourceReferenceId));
            updateApplicationRequest.SourceType = (AssignWisely (submitApplicationRequest.SourceType, applicationResponse.Source.SourceType));
            updateApplicationRequest.SystemChannel = (AssignWisely (submitApplicationRequest.SystemChannel, applicationResponse.Source.SystemChannel));
            updateApplicationRequest.TrackingCode = (AssignWisely (submitApplicationRequest.TrackingCode, applicationResponse.Source.TrackingCode));
            updateApplicationRequest.OtherPurposeDescription = (AssignWisely (submitApplicationRequest.OtherPurposeDescription, applicationResponse.OtherPurposeDescription));
            updateApplicationRequest.PromoCode = (AssignWisely (submitApplicationRequest.PromoCode, applicationResponse.PromoCode));
            updateApplicationRequest.SodexoCode = (AssignWisely (submitApplicationRequest.SodexoCode, applicationResponse.SodexoCode));
            updateApplicationRequest.TrackingCodeMedium = (AssignWisely (submitApplicationRequest.TrackingCodeMedium, applicationResponse.Source.TrackingCodeMedium));
            updateApplicationRequest.GCLId = (AssignWisely (submitApplicationRequest.GCLId, applicationResponse.Source.GCLId));
            updateApplicationRequest.TrackingCodeCampaign = (AssignWisely (submitApplicationRequest.TrackingCodeCampaign, applicationResponse.Source.TrackingCodeCampaign));
            updateApplicationRequest.TrackingCodeTerm = (AssignWisely (submitApplicationRequest.TrackingCodeTerm, applicationResponse.Source.TrackingCodeTerm));
            updateApplicationRequest.TrackingCodeContent = (AssignWisely (submitApplicationRequest.TrackingCodeContent, applicationResponse.Source.TrackingCodeContent));

            var UpdateApplicationRequest = GetApplicationRequest (new SubmitApplicationRequest (updateApplicationRequest));

            // Application Update Lead 
            IApplication application = null;
            Logger.Info ($"[OfferEngine/ApplicationService/UpdateApplication] UpdateApplication method of Application Service is called for [{applicationNumber}] application : ");
            application = await ExternalApplicationService.UpdateApplication (applicationNumber, UpdateApplicationRequest);
            Logger.Info ($"[OfferEngine/ApplicationService/UpdateApplication] UpdateApplication method of Application Service is called finshed  for [{applicationNumber}] result : [{  (application != null ? JsonConvert.SerializeObject(application) : null)}] ");
            var UpdateApplicantResponse = await ApplicantService.Get (application.ApplicantId);
            await EventHub.Publish ("UpdateLeadApplication", new { Application = application, Applicant = UpdateApplicantResponse });
            string applicationStatus = status.Name;
            await DataAttributesEngine.SetAttribute ("application", application.ApplicationNumber, "extension", GetApplicationExtension (new SubmitApplicationRequest (updateApplicationRequest), emailVerifyResponse, ApplicationProcessorConfiguration.EnablePersonalEmailVerification, ApplicationProcessorConfiguration.EmailValidationProvider.ToLower ()));
            Logger.Info ($"[OfferEngine/ApplicationService] UpdateApplication method execution finshed for [{applicationNumber}] application : [{  (application != null ? JsonConvert.SerializeObject(application) : null)}] applicationStatus:[{applicationStatus}] ");

            //throw if any data not allow to updat in current staus
            if (Message.Count > 0)
            {
                string errors = "\n ";
                foreach (var item in Message)
                {
                    errors += item + "\n ";
                }
                throw new InvalidOperationException (errors);
            }

            return new Tuple<IApplication, string> (application, applicationStatus);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="applicationNumber"></param>
        /// <param name="employmentInformationRequest"></param>
        /// <returns></returns>
        public async Task UpdateEmploymentDetail (string applicationNumber, IEmploymentDetail employmentInformationRequest, string employementYear = null, string employementMonth = null)
        {
            EnsureInputIsValid (applicationNumber, employmentInformationRequest, employementYear, employementMonth);
            var status = EntityStatusService.GetStatusByEntity ("application", applicationNumber).Result;

            var application = await ExternalApplicationService.GetByApplicationNumber (applicationNumber);
            if (application == null)
                throw new NotFoundException ($"Application with number {applicationNumber} is not found");
            //employement month and year validation
            // can not be both null
            // can not be null or zero year if month is null or zero
            if (!string.IsNullOrEmpty (employementYear))
            {
                if (employementYear == "0")
                {
                    throw new NotFoundException ("employement Year Year can not be zero");
                }
            }
            if (status != null && status.Code == ApplicationProcessorConfiguration.Statuses["Approved"])
            {
                if (string.IsNullOrWhiteSpace (employmentInformationRequest.EmploymentId))
                {
                    await ApplicantService.AddEmployment (application.ApplicantId, employmentInformationRequest);
                }
                else
                {
                    await ApplicantService.UpdateEmployment (application.ApplicantId, employmentInformationRequest.EmploymentId, employmentInformationRequest);
                }
            }
            else
            {
                //if (status != null && ApplicationProcessorConfiguration != null && !(ApplicationProcessorConfiguration.EmploymentChangeStatus.Contains(status.Code)))
                //{
                //    throw new InvalidOperationException($"Can not update employment detail when status is {status.Code}");
                //}
                await ExternalApplicationService.UpdateEmploymentDetail (applicationNumber, employmentInformationRequest);

                if (employementYear != null)
                {
                    var submitApplicationRequest = new SubmitApplicationRequest ();
                    //submitApplicationRequest.EmployementMonth = employementMonth;
                    submitApplicationRequest.EmployementYear = employementYear;
                    await UpdatePartialApplicationData (applicationNumber, submitApplicationRequest);
                }
            }
        }

        public static bool CheckNullOrEmpty<T> (T value)
        {
            if (typeof (T) == typeof (string))
                return string.IsNullOrEmpty (value as string);
            return value == null || value.Equals (default (T));
        }
        private T AssignWisely<T> (T Newvalue, T OldValue)
        {
            if (typeof (T) == typeof (int) || typeof (T) == typeof (double))
            {
                if (Newvalue.Equals (0))
                {
                    return OldValue;
                }
            }
            return (!CheckNullOrEmpty (Newvalue)) ? Newvalue : (OldValue);
        }
        #region Execute Request
        private T ExecuteRequest<T> (string response) where T : class
        {
            try
            {
                return JsonConvert.DeserializeObject<T> (response);
            }
            catch (Exception exception)
            {
                throw new Exception ("Unable to deserialize:" + response, exception);
            }
        }
        #endregion
    }
}