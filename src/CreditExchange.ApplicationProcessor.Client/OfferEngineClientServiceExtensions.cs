﻿using LendFoundry.Security.Tokens;
using Microsoft.Extensions.DependencyInjection;

namespace CreditExchange.ApplicationProcessor.Client
{
    public static class OfferEngineClientServiceExtensions
    {
        public static IServiceCollection AddOfferEngineService(this IServiceCollection services, string endpoint, int port = 5000)
        {
            services.AddTransient<IOfferEngineClientServiceFactory>(p => new OfferEngineClientServiceFactory(p, endpoint, port));
            services.AddTransient(p => p.GetService<IOfferEngineClientServiceFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }
    }
}