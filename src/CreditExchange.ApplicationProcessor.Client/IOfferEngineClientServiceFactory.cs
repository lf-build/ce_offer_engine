﻿using LendFoundry.Security.Tokens;

namespace CreditExchange.ApplicationProcessor.Client
{
    public interface IOfferEngineClientServiceFactory
    {
        IOfferEngineService Create(ITokenReader reader);
    }
}
